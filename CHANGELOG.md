# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2021-06-16

### Added

- Add Water quality Icon and label text selection via switches
- Add Export CSV  to plot data
- Add Spinner icon to API data load section
- Add color picker to Water quality stations when Icon is disabled
- Add image html preload
- Add Lazy loading to Console

### Changed

- Move css declarations to seperated files on commons components

### Removed

### Fixed

- Fix css responsiveness on Map settings and Search

## [1.0.0] - 2021-06-14

### Added

- Spatial visualisation of Water infrastructures, Water Quality stations and Water links using Google Map API
- Water quality data visualisation using Nivo: Bar, Bubble and Line
- Upload Component that handles multiple file types
- Search option for loaded infrastructure data
- Flexibe App layout using react-grid-layout

### Changed

### Removed

### Fixed