
## Getting Started



### Prerequisites

-   Suported OS
    -   MacOS
    -   Linux

- Pre-installation requirements
    -   NodeJS
    -   npm


### Installation


- Install application:
    -   `cd frontend-ediawater/ediawater`
    -   `npm install -E`
- Test installation:
    -   `npm start`

    

## Deployment

Test `webpack` configuration at your local machine:

- Generate build folder

   - `npm run dev-build`
- Start webpack server

   - `npm run dev-start`

If the chosen deployment option is the built-in create-react-app script (`react-scripts build`), test the deployment with the [serve package](https://www.npmjs.com/package/serve): `npm run dev-deploy`. Also, make sure that you have the correct `homepage` option at `package.json` (or in your `.env` with the correct url assigned to `PUBLIC_URL` variable).

#### [Heroku](https://devcenter.heroku.com/articles/git)

1.  Create a App
    -   Save the App name
2.  Go to Account settings
    1.   Get  your API token
    2.  Set your environment variables
3.  Add remote Heroku repo to the project folder `ediawater`
4.  Git push all files 



## Contributing

### Conventions

The [Google's JavaScript Style Guide ](https://google.github.io/styleguide/jsguide.html) is the main convention applied to the development of this project.  

- Style
  - [Prettier](https://prettier.io/docs/en/index.html)
- Documentation
  - [JSDoc]([JSDoc](https://jsdoc.app/about-commandline.html))
    - [Wordpress JavaScript Doc Standards](https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/)

### Testing

- Linting
  - ` npm run code-format-fix && npm run code-format-check` 
  - `npm run code-style-fix && npm run code-style-check `



### Git branches and CI pipeline

#### Git branch convention

###### Principal

-   `master` : current stable version ready to be deployed. All stable and mature code at `develop` branch will be merged to `master`.
-   `develop` : beta version and current development version.

###### Support

-   `feature-*` : new features development branch. the naming should be coherent with the scope , ex: ``feature-<framework>-<functionality>`. the features branches are used to integrate a new feature to the development branch.
-   hot-fix branches are created via issue's auto merge requests. See [Create a new branch from an issue](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-an-issue)



#### CI pipeline

-   `develop`

    ​	linting >> testing >> deploy

-   `master`

    ​	linting >> testing >> release >> deploy

    If the deployment is successful at `develop` branch and the current version on master (after the merging from `develop`)  checks all requirements,  a new release tag should  be created.  The release job is only triggered if a tag is (manually) pushed to the repository. 

#### Versioning

The tag creation is manual and the tag convention for a deployed version from `master` ( referred deploy to an infrastructure) is `v.{major}.{minor}.{revision/patch}` . The `<major>` is the version of current development and `<minor>` is the version's iteration.

##### Git release tag

-   show existing tags: `git tag`
-   create tag: `git tag -a <tag> -m <text> ; Where` `<tag> ` can be ` v1.4` (`v.{major}.{minor}.{revision}`) and  `<text>` as annotation "my version 1.4.0"`
-   see the new tag: `git show <tag>`
-   git push the new tag: `git push -u <tag>` or `git push -u --tags`


## License

See the [LICENSE](LICENSE) file for more details.

## Acknowledgments


-	[react-google-maps-api](https://react-google-maps-api-docs.netlify.app/)

-	[react-bootstrap](https://react-bootstrap.github.io/)

-	[geojson.io](https://geojson.io/)
-	[mapcoordinates.net](https://www.mapcoordinates.net/en)