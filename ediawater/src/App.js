/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo    Add Context API to theme change
 * @todo
 */

import React, { Component } from 'react'
import RGL, { WidthProvider } from 'react-grid-layout'
// import { Form, Image, Container, Col, Row } from "react-bootstrap";

import './styles.css'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/css/bootstrap.css' // or include from a CDN
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css'

// Components
import { AppLayout } from './Layout'
import { Nav } from './Components/Navbar/Navbar'
import { Exit } from './Components/Commons/Exit'
import { Fasten } from './Components/Commons/Fasten'

// Functions

// Constants
const GridLayout = WidthProvider(RGL)
const DefaultThemeName = '' // or 'Dark'

export const API_SERVER_URL = process.env.REACT_APP_API_HOST
  ? process.env.REACT_APP_API_HOST
  : 'http://localhost:5001/'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      layout: [...AppLayout.layout],
      items: [...AppLayout.items],
      theme: DefaultThemeName
    }

    this.AddItemHandler = this.AddItemHandler.bind(this)
    this.ChangeTheme = this.onChangeTheme.bind(this)
  }

  onChangeTheme = (event) => {
    if (event.target.checked) this.setState({ theme: event.target.value })
    else this.setState({ theme: DefaultThemeName })
  };

  /**
    This function will update the react-layout-grid static option on the bind element
      @param {string} eventKey - event key from the navbar
      @returns {object} l - layout element
  */
  onToggleStatic (elem) {
    this.setState({
      layout: this.state.layout.map((l) => {
        if (l.i === elem.name) {
          l.static = !l.static
          console.log('toggle', l.static, elem.name)
        }
        return l
      })
    })
  }

  // - Tentar que seja adicionado no fim, ver qual o maior e fazer update no layout
  // - Perceber o  porquê do import/export ficar sem resizable  e porque que o tamanho não é adicionado
  // - usar hashmap Map() para facilitar a procura do layout correspondente pelo nome

  onRemoveItem (elem) {
    console.log('removing', elem)

    this.setState({
      items: this.state.items.filter((i) => i.name !== elem.name)
    })
    this.setState({
      layout: this.state.layout.filter((l) => l.i !== elem.name)
    })
  }

  AddItemHandler (eventKey) {
    console.log('Evente key?', eventKey)
    if (
      this.state.items.filter(Boolean).find((item) => item.key === eventKey)
    ) {
      alert('Already exists')
    } else {
      const newItem = AppLayout.items.find(
        (newItem) => newItem.key === eventKey
      )
      const newItemlayout = AppLayout.layout.find(
        (newLayout) => newLayout.i === newItem.name
      )

      this.setState({ layout: [newItemlayout, ...this.state.layout] })
      this.setState({ items: [newItem, ...this.state.items] })
      console.log('Add', newItem, newItemlayout)
    }
  }

  render () {
    const elements = this.state.items

    return (
      <div className={'App' + this.state.theme}>
        <Nav
          AddItemHandler={this.AddItemHandler}
          handleChangeThemeHandler={this.onChangeTheme}
          theme={this.state.theme}
        />
        <div>
          <>
            <GridLayout
              rowHeight={AppLayout.rowHeight}
              layout={AppLayout.layout}
              cols={AppLayout.cols}
              breakpoints={AppLayout.breakpoints}
              onLayoutChange={(layout) => this.setState({ layout })}
            >
              {elements.map((elem) => (
                <div className={'Card' + this.state.theme} key={elem.name}>
                  {elem.comp(this.state.theme)}
                  <Exit clickBind={this.onRemoveItem.bind(this, elem)} />
                  <Fasten clickBind={this.onToggleStatic.bind(this, elem)} />
                </div>
              ))}
            </GridLayout>
          </>
        </div>
      </div>
    )
  }
}

export default App
