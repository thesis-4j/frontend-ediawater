import React, { useState, useLayoutEffect } from "react";

import "./ColorPicker.css";

export const ColorPicker = ({ handlePickColor, intialValue, text }) => {
  const [color, setColor] = useState("#FFFFFF");

  useLayoutEffect(() => {
    if (intialValue) setColor(intialValue);
  }, [intialValue]);

  return (
    <span className={"ColorSectionComp"}>
      {text ? "Color" : null}
      <span className={"ColorPicker"}>
        <input
          type="color"
          value={color}
          onChange={(e) => {
            setColor(e.target.value);
            if (handlePickColor) handlePickColor(e.target.value);
          }}
          className={"ColorInput"}
        />
        <input
          type="text"
          value={color}
          onChange={(e) => {
            setColor(e.target.value);
            if (handlePickColor) handlePickColor(e.target.value);
          }}
          className={"ColorText"}
        />
      </span>
    </span>
  );
};

const ColorPickerMemo = React.memo(ColorPicker);

export default ColorPickerMemo;
