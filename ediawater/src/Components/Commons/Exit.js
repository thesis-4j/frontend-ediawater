import React, { Component } from "react";
import "./Exit.css";

export class Exit extends Component {
  render() {
    return (
      <span className="Remove" onClick={this.props.clickBind}>
        x
      </span>
    );
  }
}
