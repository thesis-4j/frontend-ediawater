import React, { Component } from "react";
import { Form } from "react-bootstrap";

export class ThemeSwitch extends Component {
  render() {
    return (
      <Form.Check
        type="switch"
        id="theme-switch"
        label="Dark Mode"
        value="Dark"
        onChange={this.props.onChangeTheme.bind(this)}
        inline
      />
    );
  }
}
