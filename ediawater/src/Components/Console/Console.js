/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo receive data via props handler
 * @todo
 */

/** jshint {inline configuration here} */

import React, { Component } from "react";
import { Nav } from "react-bootstrap";

import { jsonToCSV } from "react-papaparse";
import { saveAs } from "file-saver";

import { FileImport } from "./Import";
import { DataConsole } from "./DataConsole/DataConsole";
import { PlotOptionsMemo } from "./ConsolePlotOption";

import { filter } from "rxjs/operators";
import { DataTopic, mainSubject } from "../../PubSub/PubSub";

export class Console extends Component {
  static defaultProps = {
    theme: "",
  };

  unsub = null;

  constructor(props) {
    super(props);

    this.state = {
      render: "default",
      plotOptionRequest: null, //Request header from DataConsole
      renderPlotOptions: null, //Selected at DataConsole
      theme: this.props.theme,
      topic: DataTopic,
      data: [],
      plot: null,
    };

    this.ChildElement = React.createRef();

    this.handleSelector = this.handleSelector.bind(this);
    this.handlePlotSelect = this.handlePlotSelect.bind(this);
    this.handleSetPlotOptions = this.handleSetPlotOptions.bind(this);
    this.handleExportEvent = this.handleExportEvent.bind(this);
  }

  componentDidUpdate() {
    console.log("Console.js did update");
    // console.log("received data", this.state.data);
  }

  componentDidMount() {
    this.unsub = mainSubject
      .pipe(filter((f) => f.topic === this.state.topic))
      .subscribe((s) => {
        this.setState({ data: s.data });
      });
  }

  componentWillUnmount() {
    this.unsub.unsubscribe();
  }

  //  The options object will be where the user will insert the react object to be rendered.
  //  the  keys should be the same as the event key from the navbar
  renderSettings = (parseData, plotOpt, handleReturn, dataStateRef) => {
    console.log("plotOpt", plotOpt);
    return {
      import: <FileImport />,
      default: (
        <DataConsole
          data={parseData}
          handlePlot={handleReturn}
          plotOption={plotOpt}
          ref={dataStateRef}
        />
      ),
      guest: <p>Hello, you will need to login first!!</p>,
    };
  };

  /**
  This function will receive a event key and will save as state the respective option object
    @param {string} eventKey - event key from the navbar
  */
  handleSelector(eventKey) {
    try {
      if (this.state.render === eventKey) {
        this.setState({ render: "default" });
      } else {
        this.setState({ render: eventKey });
      }
    } catch (error) {
      this.setState({ render: "default" });
    }
  }

  handlePlotSelect(eventKey) {
    console.log("Console", eventKey);
    if (this.state.renderPlotOptions !== eventKey.plot) {
      this.setState({
        renderPlotOptions: eventKey.plot,
        plotOptionRequest: eventKey.request,
      });
    } else this.setState({ renderPlotOptions: null, plotOptionRequest: null });
  }

  handleExportEvent(eventKey) {
    const childelement = this.ChildElement.current;
    console.log(childelement.state);

    if (childelement.state.plotType && childelement.state.plotData) {
      try {
        if (childelement.state.plotData[childelement.state.plotType]) {
          if (
            childelement.state.plotType === "point" &&
            childelement.state.plotData[childelement.state.plotType].data.length
          ) {
            const csv = jsonToCSV(
              childelement.state.plotData[childelement.state.plotType].data
            );
            const csvData = new Blob([csv], {
              type: "text/csv;charset=utf-8;",
            });
            saveAs(csvData, "data-bar.csv");
          } else if (
            childelement.state.plotType === "tseries" &&
            childelement.state.plotData[childelement.state.plotType].length
          ) {
            let objList = [];
            let data = childelement.state.plotData[childelement.state.plotType];
            for (var i = data.length - 1; i >= 0; i--) {
              let id = data[i].key;
              let nameLength = data[i].id.length - id.length -1
              let name = data[i].id.slice(0, nameLength);
              for (var j = data[i].data.length - 1; j >= 0; j--) {
                data[i].data[j]["id"] = id;
                data[i].data[j]["name"] = name;
              }
              objList = [...objList, ...data[i].data];
            }
            const csv = jsonToCSV(objList);
            const csvData = new Blob([csv], {
              type: "text/csv;charset=utf-8;",
            });
            saveAs(csvData, "data-tseries.csv");
          } else {
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  }

  handleSetPlotOptions(obj) {
    //console.log("SET", obj);
    this.setState({ plot: obj });
  }

  render() {
    //console.log("SET on render", this.state.plot);
    return (
      <div className="Console">
        <div className={"NavFile" + this.props.theme}>
          <Nav>
            <span className="NavButtons">
              <Nav.Item>
                <Nav.Link eventKey="import" onSelect={this.handleSelector}>
                  Import
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="export" onSelect={this.handleExportEvent}>
                  Export
                </Nav.Link>
              </Nav.Item>
            </span>
            <span className="NavOptions">
              <PlotOptionsMemo
                option={this.state.renderPlotOptions}
                dateRequest={this.state.plotOptionRequest}
                onHandleSetOptions={this.handleSetPlotOptions}
              />
            </span>
          </Nav>
        </div>
        <div className="ConsoleRender">
          {
            this.renderSettings(
              this.state.data,
              this.state.plot,
              this.handlePlotSelect,
              this.ChildElement
            )[this.state.render]
          }
        </div>
      </div>
    );
  }
}

export default Console;
