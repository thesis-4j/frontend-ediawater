import React, { useState, useEffect } from "react";
import InputRange from "react-input-range";
import { Button } from "react-bootstrap";
import moment from "moment";
import axios from "axios";

import { API_SERVER_URL } from "../../App";

import "react-input-range/lib/css/index.css";

function convertDate(date) {
  return parseInt(date.format("x"), 10);
}

async function fetchData(jsonReq, setLimit, setVal, setLoading) {
  setLoading(true);
  await axios
    .post(API_SERVER_URL + "api/water-quality/attr/data/dates", jsonReq, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((res) => {
      let { data } = res;
      if (typeof data !== "undefined" && data.length) {
        console.log(
          "min",
          data[0].dates[0],
          "max",
          data[0].dates[data[0].dates.length - 1]
        );

        if (data[0].dates[0] && data[0].dates[data[0].dates.length - 1]) {
          const value = {
            min: convertDate(moment(data[0].dates[0], "DD/MM/YYYY")),
            max: convertDate(
              moment(data[0].dates[data[0].dates.length - 1], "DD/MM/YYYY")
            ),
          };
          setLimit(value);
          setVal(value);
          setLoading(false);
        }
      }
    })
    .catch((error) => {
      console.log(error);
      setLoading(false);
    });
}

const PlotOptions = ({ onHandleSetOptions, option, dateRequest }) => {
  const [optset, setOpt] = useState("avg");
  const [parseStatus, setParse] = useState(true); // used on timeseries to execute conditional parsing at Dataconsole
  const [loandingStatus, setLoading] = useState(false);
  const [update, updateRequired] = useState(false);
  const [getRangeStatus, getRange] = useState(false); // button to enable  input range scope

  const [limit, setLimit] = useState({
    min: convertDate(moment()),
    max: convertDate(moment().add(2, "M")),
  });
  const [rangeVal, setVal] = useState({
    min: convertDate(moment()),
    max: convertDate(moment().add(10, "d")),
  });

  let lastRequest = "";

  useEffect(() => {
    if (getRangeStatus) {
      setOpt(null);
      updateRequired(true);
    }
  }, [rangeVal, getRangeStatus, dateRequest]);

  return (
    <div className={"optionSelContainer"}>
      {option && (
        <span className={"optionSelItemGroup"}>
          <Button
            variant="primary"
            onClick={(e) => {
              onHandleSetOptions({
                option: optset,
                range: getRangeStatus,
                values: rangeVal,
                parse: parseStatus,
              });
              updateRequired(false);
            }}
          >
            {update ? (
              "SET"
            ) : (
              <span style={{ fontWeight: "bold" }}> #{option}</span>
            )}
          </Button>
        </span>
      )}

      {option === "point" && (
        <span className={"optionSelItemGroup"}>
          <Button
            variant="info"
            name="avg"
            size={"sm"}
            active={optset === "avg"}
            onClick={() => {
              setOpt("avg");
              updateRequired(true);
            }}
          >
            AVG
          </Button>
          <Button
            variant="info"
            name="max"
            size={"sm"}
            active={optset === "max"}
            onClick={() => {
              setOpt("max");
              updateRequired(true);
            }}
          >
            MAX
          </Button>
          <Button
            variant="info"
            name="min"
            size={"sm"}
            active={optset === "min"}
            onClick={() => {
              setOpt("min");
              updateRequired(true);
            }}
          >
            MIN
          </Button>
        </span>
      )}

      {(option === "point" || option === "tseries") && (
        <span className={"optionSelItemGroup"}>
          <Button
            variant="info"
            name="sub"
            size={"sm"}
            active={optset === "sub"}
            onClick={() => {
              setOpt("sub");
              updateRequired(true);
            }}
            disabled
          >
            SUB
          </Button>
          <Button
            variant="info"
            name="add"
            size={"sm"}
            active={optset === "add"}
            onClick={() => {
              setOpt("add");
              updateRequired(true);
            }}
            disabled
          >
            ADD
          </Button>
        </span>
      )}
      {option === "tseries" && (
        <span className={"optionSelItemGroup"}>
          <Button
            variant="secondary"
            name="sub"
            size={"sm"}
            active={parseStatus}
            onClick={() => setParse((prevState) => !prevState)}
          >
            PARSE
          </Button>
        </span>
      )}

      {(option === "point" || option === "heat" || option === "tseries") && (
        <span className={"optionSelItemGroup"}>
          <Button
            variant="danger"
            name="range"
            size={"sm"}
            onClick={() => {
              getRange((prevState) => !prevState);
              if (dateRequest && dateRequest.length > 0 && !loandingStatus) {
                const jsonR = JSON.stringify(dateRequest);
                if (jsonR !== lastRequest) {
                  fetchData(jsonR, setLimit, setVal, setLoading);
                  lastRequest = jsonR;
                }
              }
            }}
          >
            SCOPE
          </Button>

          <span className={"RangeInputNavConsole"}>
            {getRangeStatus && (
              <InputRange
                draggableTrack
                allowSameValues
                minValue={limit.min}
                maxValue={limit.max}
                value={
                  rangeVal > limit.max || rangeVal < limit.min
                    ? limit.min
                    : rangeVal
                }
                onChange={(value) => {
                  let checkValue = { ...value };
                  if (value.min < limit.min) {
                    checkValue.min = limit.min;
                  }
                  if (value.max > limit.max) {
                    checkValue.max = limit.max;
                  }
                  setVal(checkValue);
                  setOpt("range");
                }}
                formatLabel={(value) => moment(value).format("DD/MM/YYYY")}
              />
            )}
          </span>
        </span>
      )}
    </div>
  );
};

export const PlotOptionsMemo = React.memo(PlotOptions);

export default PlotOptionsMemo;
