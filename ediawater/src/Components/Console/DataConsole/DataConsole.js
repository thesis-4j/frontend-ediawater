import React, { PureComponent } from "react";
import { Container, Row, Col, Button, Spinner } from "react-bootstrap";
import axios from "axios";
import { fromJS } from "immutable";

import { API_SERVER_URL } from "../../../App";
import { isEmpty, ExistsInList, remDups } from "../../../Libraries/UtilsLib";
// Components

import RegexInputMemo from "./RegexInput";
import DataViewer from "./DataViewer";
import PlotHOC from "./Plot/PlotHOC";
import PropertiesTabMemo from "./PropertiesTab";
import Paginator from "./Paginator";

// Constants

const endpointUrl = {
  point: {
    url: "api/water-quality/obj/attr/data/",
    option: { min: "min", avg: "avg", max: "max" },
    default: "avg",
  },
  tseries: { url: "api/water-quality/timeseries/attr/data" },
  bubble: { url: "api/water-quality/bubble/attr/data" },
  attr: { url: "api/water-quality/multiple/attr" },
};

export class DataConsole extends PureComponent {
  // Used for unsubscribing when our components unmount
  unsub = null;

  static defaultProps = {
    data: [],
    plotOption: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      prevDataImm: {},
      dataRemoveUndo: null,

      qualityStationsIDsFromData: [],
      properties: [], // All properties ex { name: "123", status: false, count: 1 }
      propertiesSelected: [], // All selected properties from this.state.properties
      propertiesRegex: {}, // ex { property: regexRule }

      plotData: { point: { data: [], keys: [] }, tseries: [], bubble: {} }, // Parsed data of selected properties
      plotType: null,

      filterProperty: "",
      tabDisplay: "dataviewer",

      loadingAttr: false,
      loadingPlot: false,
    };

    this.handleTableRowClick = this.handleTableRowClick.bind(this);
    this.getAttrAPI = this.getAttrAPI.bind(this);
    this.getDataPointBarAPI = this.getDataPointBarAPI.bind(this);
    this.getTimeSeriesDataAPI = this.getTimeSeriesDataAPI.bind(this);
    this.handlePropViewerInput = this.handlePropViewerInput.bind(this);
    this.handlePlotButtonsOption = this.handlePlotButtonsOption.bind(this);
    this.handleSelectProperty = this.handleSelectProperty.bind(this);
    this.handleSetTabDisplay = this.handleSetTabDisplay.bind(this);
    this.handleClickToCallApi = this.handleClickToCallApi.bind(this);
    this.getBubbleDataAPI = this.getBubbleDataAPI.bind(this);
    this.buildRequestHeaders = this.buildRequestHeaders.bind(this);

    this.SelectCallAPI = {
      point: this.getDataPointBarAPI,
      tseries: this.getTimeSeriesDataAPI,
      bubble: this.getBubbleDataAPI,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const imm = fromJS(nextProps.data);

    if (!imm.equals(prevState.prevDataImm)) {
      return {
        data: [...prevState.data, ...nextProps.data],
        prevDataImm: imm,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("DataConsole.js updated");
  }

  parsePointDataRegex(data, regexProperties) {
    const regexKeys = Object.keys(regexProperties);
    let parseVal;
    console.log("regex data", data);
    for (var i = data.length - 1; i >= 0; i--) {
      for (var j = regexKeys.length - 1; j >= 0; j--) {
        try {
          parseVal = parseFloat(
            data[i][regexKeys[j]].match(regexProperties[regexKeys[j]])[0]
          );
          if (parseVal) {
            data[i][regexKeys[j]] = parseVal;
          }
        } catch (err) {
          console.log("shit on match", err);
        }
      }
    }
    return data;
  }

  parseTimeSeriesRegex(dataList, regexProperties) {
    const keys = Object.keys(regexProperties);
    let parseVal;

    for (var i = dataList.length - 1; i >= 0; i--) {
      if (ExistsInList(keys, dataList[i].id, (key, id) => key === id)) {
        for (var j = dataList[i].data.length - 1; j >= 0; j--) {
          try {
            parseVal = parseFloat(
              dataList[i].data[j].y.match(regexProperties[dataList[i].id])[0]
            );
            if (parseVal) {
              dataList[i].data[j].y = parseVal;
            }
          } catch (err) {
            console.log("shit on match", err);
          }
        }
      }
    }

    return dataList;
  }

  removeDuplicatedData(dataList) {
    for (var i = dataList.length - 1; i >= 0; i--) {
      dataList[i].data = remDups(dataList[i].data, ["x", "y"], true, true);
    }
  }

  buildRequestHeaders() {
    const selectedAttr = this.state.propertiesSelected.map((e) => e.name);
    const requestAttrData = this.state.data.map((e) => ({
      id: e.id,
      attributes: selectedAttr,
    }));

    return requestAttrData;
  }

  async getDataPointBarAPI() {
    // [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]}, ...

    if (!this.state.loadingPlot) {
      let url = endpointUrl["point"].url;
      console.log(this.props.plotOption);
      if (this.props.plotOption && this.state.plotType === "point") {
        let opt = endpointUrl["point"].option[this.props.plotOption.option];
        if (opt) {
          url += opt;
        } else {
          url += endpointUrl["point"].default;
        }
      } else {
        url += endpointUrl["point"].default;
      }
      console.log("Final url", url);

      this.setState({ loadingPlot: true });
      const jsonReq = JSON.stringify(this.buildRequestHeaders());
      await axios
        .post(API_SERVER_URL + url, jsonReq, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => {
          const { data } = res;
          console.log("getDataPointBarAPI()", data);
          if (typeof data !== "undefined" && data.length) {
            if (!isEmpty(this.state.propertiesRegex)) {
              data[0].data = this.parsePointDataRegex(
                data[0].data,
                this.state.propertiesRegex
              );
            }

            this.setState({
              tabDisplay: "plot",
              plotData: {
                ...this.state.plotData,
                point: { data: data[0].data, keys: data[0].keys },
              },
              loadingPlot: false,
            });
          } else {
            this.setState({ loadingPlot: false });
          }
        })
        .catch((error) => {
          console.log(error);
          this.setState({ loadingPlot: false });
        });
    }
  }

  async getBubbleDataAPI() {
    // [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]}, ...

    if (!this.state.loadingPlot) {
      this.setState({ loadingPlot: true });
      const jsonReq = JSON.stringify(this.buildRequestHeaders());
      await axios
        .post(API_SERVER_URL + endpointUrl["bubble"].url, jsonReq, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => {
          const { data } = res;
          console.log("getBubbleDataAPI()", data);
          if (typeof data !== "undefined" && data.length) {
            //should do regex parsing here

            let object = { id: "data", children: data };

            this.setState({
              tabDisplay: "plot",
              plotData: { ...this.state.plotData, bubble: object },
              loadingPlot: false,
            });
          } else {
            this.setState({ loadingPlot: false });
          }
        })
        .catch((error) => {
          console.log(error);
          this.setState({ loadingPlot: false });
        });
    }
  }

  async getTimeSeriesDataAPI() {
    if (!this.state.loadingPlot) {
      this.setState({ loadingPlot: true });
      const jsonReq = JSON.stringify(this.buildRequestHeaders());
      await axios
        .post(API_SERVER_URL + endpointUrl["tseries"].url, jsonReq, {
          // {id:"WQS_RPRISNIRH_18" , data [{date, name, value}]}
          headers: {
            "Content-Type": "application/json", //target  {}
          },
        })
        .then((res) => {
          let { data } = res;
          console.log("getTimeSeriesDataAPI()", data);
          if (typeof data !== "undefined" && data.length) {
            if (!isEmpty(this.state.propertiesRegex)) {
              data = this.parseTimeSeriesRegex(
                data,
                this.state.propertiesRegex
              );
            }

            if (
              (this.props.plotOption &&
                this.props.plotOption.option === "tseries" &&
                this.props.plotOption.parse) ||
              this.props.plotOption === null ||
              typeof this.props.plotOption === "undefined"
            ) {
              this.removeDuplicatedData(data);
            }

            this.setState({
              tabDisplay: "plot",
              plotData: { ...this.state.plotData, tseries: data },
              loadingPlot: false,
            });
          } else {
            this.setState({ loadingPlot: false });
          }
        })
        .catch((error) => {
          console.log(error);
          this.setState({ loadingPlot: false });
        });
    }
  }

  async getAttrAPI(prevState) {
    const ids = this.state.data.map((e) => e.id);
    const immId = fromJS(ids);

    if (!immId.equals(this.state.qualityStationsIDsFromData)) {
      const jsonids = JSON.stringify(ids);
      console.log("what json:", jsonids);

      if (!this.state.loadingAttr) {
        this.setState({ loadingAttr: true });

        await axios
          .post(API_SERVER_URL + endpointUrl["attr"].url, jsonids, {
            headers: {
              "Content-Type": "application/json",
            },
          })
          .then((res) => {
            const { data } = res;

            if (typeof data !== "undefined" && data.length) {
              console.log("api data", data, immId);
              this.setState({
                properties: data,
                propertiesSelected: [],
                qualityStationsIDsFromData: immId,
                loadingAttr: false,
              });
            } else {
              this.setState({ loadingAttr: false });
            }
          })
          .catch((error) => {
            this.setState({ loadingAttr: false });
            console.log(error);
          });
      }
    }
  }

  handleSelectProperty(elem) {
    console.log("status defined", elem.status);
    if (elem.status === undefined) elem["status"] = true;
    else elem.status = !elem.status;

    if (elem.status) {
      if (
        !ExistsInList(this.state.propertiesSelected, elem, (obj, spec) =>
          obj.name && obj.name === spec.name ? true : false
        )
      ) {
        this.setState({
          propertiesSelected: [...this.state.propertiesSelected, elem],
        });
      }
    } else {
      this.setState({
        propertiesSelected: this.state.propertiesSelected.filter(
          (e) => e.name !== elem.name
        ),
      });
    }
  }

  handleTableRowClick(key) {
    console.log(
      "handleTable",
      key,
      this.state.data[key],
      this.state.data.length
    );

    const e = this.state.data.splice(key, 1);

    this.setState({
      dataRemoveUndo: e,
    });
  }

  handlePlotButtonsOption(option) {
    console.log("Plot option:", option);
    this.setState({ plotType: option.plot });
    this.props.handlePlot(option);
  }

  handlePropViewerInput(e) {
    this.setState({ filterProperty: e.target.value });
  }

  handleClickToCallApi(e) {
    this.getAttrAPI();
  }

  handleSetTabDisplay(displayOption) {
    this.setState({ tabDisplay: displayOption });
  }

  render() {
    return (
      <div>
        <Container
          fluid
          style={{ paddingLeft: 0, paddingRight: 0 }}
          className="DataContainer"
        >
          <Row noGutters>
            <Col sm={8} className="ConsoleTable">
              <div>
                {this.state.tabDisplay === "dataviewer" ? (
                  <DataViewer
                    data={this.state.data}
                    rowClickHandler={this.handleTableRowClick}
                  />
                ) : (
                  <div
                    style={{ height: 600, overflow: "scroll", width: "100%" }}
                  >
                    <PlotHOC
                      plotData={this.state.plotData[this.state.plotType]}
                      plotOption={this.state.plotType}
                    />
                  </div>
                )}
              </div>
            </Col>
            <Col sm={4} className={"PropertiesView"}>
              <div>
                <div className={"PropertiesViewTab"}>
                  <PropertiesTabMemo
                    handlePropViewerInput={this.handlePropViewerInput}
                    handleSelectProperty={this.handleSelectProperty}
                    handleClickInput={this.handleClickToCallApi}
                    properties={this.state.properties}
                    selectedProperties={this.state.propertiesSelected}
                    filterProperty={this.state.filterProperty}
                    loadingState={this.state.loadingAttr}
                  />
                </div>

                <div className={"PropertiesViewRegexTab"}>
                  <RegexInputMemo
                    properties={this.state.propertiesSelected} //will update this array object
                    propertiesRegex={this.state.propertiesRegex}
                  />
                </div>

                <div className={"PropertiesViewVizTab"}>
                  <div>
                    <h6>Visualization</h6>
                  </div>
                  <div className={"VizTabButtons"}>
                    <Button
                      variant="primary"
                      onClick={() =>
                        this.handlePlotButtonsOption({
                          plot: "heat",
                          request: null,
                        })
                      }
                      disabled
                    >
                      Heatmap
                    </Button>
                    <Button
                      variant="primary"
                      onClick={() => {
                        const obj = {
                          plot: "point",
                          request: this.buildRequestHeaders(),
                        };
                        this.handlePlotButtonsOption(obj);
                      }}
                    >
                      Bar
                    </Button>
                    <Button
                      variant="primary"
                      onClick={() => {
                        const obj = {
                          plot: "tseries",
                          request: this.buildRequestHeaders(),
                        };
                        this.handlePlotButtonsOption(obj);
                      }}
                    >
                      Time Series
                    </Button>
                    <Button
                      variant="primary"
                      onClick={() =>
                        this.handlePlotButtonsOption({
                          plot: "bubble",
                          request: null,
                        })
                      }
                    >
                      Bubble
                    </Button>
                  </div>
                  <div style={{ margin: "5%" }}>
                    <Button
                      variant="success"
                      style={{ margin: "1%", width: "30%" }}
                      onClick={() => {
                        if (this.state.plotType !== null) {
                          this.SelectCallAPI[this.state.plotType]();
                        } else {
                          alert(
                            "Before you begin plotting, please select a chart type."
                          );
                        }
                      }}
                    >
                      PLOT
                      {this.state.loadingPlot && (
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                        />
                      )}
                    </Button>
                  </div>
                </div>
              </div>
              <div className={"PaginationConsole"}>
                <Paginator
                  handleSetTab={this.handleSetTabDisplay}
                  tabDisplay={this.state.tabDisplay}
                />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
