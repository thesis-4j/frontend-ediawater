import React, { Component } from "react";
import { isEmpty } from "../../../Libraries/UtilsLib";
import { Table } from "react-bootstrap";
import isEqual from "react-fast-compare";

export class DataViewer extends Component {
  static defaultProps = {
    data: [],
  };

  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  componentDidUpdate() {
    console.log("DataViewer updated");
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.data.length !== prevState.data.length) {
      return {
        data: nextProps.data,
      };
    } else {
      for (var i = nextProps.data.length - 1; i >= 0; i--) {
        if (!isEqual(nextProps.data[i], prevState.data[i]))
          return {
            data: nextProps.data,
          };
      }
    }
    return null;
  }

  render() {
    return (
      <div className={"TableData"}>
        <Table responsive="lg" striped borderless hover>
          <tbody>
            {this.state.data.map((elem, ids) => {
              if (!isEmpty(elem)) {
                return (
                  <tr key={ids} onClick={() => this.props.rowClickHandler(ids)}>
                    <td>{ids + 1}</td>
                    {Object.keys(elem).map(function (value, idx) {
                      return (
                        <td key={idx}>
                          {value}:{elem[value]}
                        </td>
                      );
                    })}
                  </tr>
                );
              } else {
                return null;
              }
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default DataViewer;
