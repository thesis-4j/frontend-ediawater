import { pushSorted } from "../../../../Libraries/UtilsLib";

//import { res } from "../../../../Libraries/apiData"; //Now at Dummy folder
//import { res2 } from "../../../../Libraries/apiData2"; //Now at Dummy folder

const pick = require("object.pick"); //base vale for testbench
/**
 * 	Example Get properties
 * 		var getLocalData = new ReadLocal();
 * 		let propArr = getLocalData.readArrayProperties(res2);
 * 		if (propArr.length) this.setState({ properties: propArr });
 *
 *	Example Get Data
 * 		var getLocalData = new ReadLocal();
 * 		const dataPlot = getLocalData.getDataToPlot(res2,this.state.propertiesSelected);
 * 		this.setState({tabDisplay: "plot",plotData: dataPlot });
 **/

const DEFAULT_REGEX_STRING = "[0-9]+[.,]?[0-9]*"; //str.match(DEFAULT_REGEX_STRING)

export class ReadLocal {
  constructor(defaultRegex = DEFAULT_REGEX_STRING) {
    this.DEFAULT_REGEX_STRING = defaultRegex;
  }

  /**
	 * readArrayProperties() method receives a array  of water quality station objects.
	 * The objects should have a key with  data from all collected water analysis
	 This key will be 'collect' by defualt, but can be also provider as parameter

		 * The initial approach
	      let propArr = [];
	      for (var i = res2.length - 1; i >= 0; i--) {
	        this.parseProperties(res2[i].collect, propArr);
	      }
	       this.setState({ properties: propArr });

	     * CURRENT usage of readArray()

	     	let arr = readArray(res2)
	     	if (arr.length)
	     		this.setState({ properties: arr });
	*/
  readArrayProperties(dataArray, keyData = "collect") {
    let propArr = [];
    for (var i = dataArray.length - 1; i >= 0; i--) {
      this.parseProperties(dataArray[i][keyData], propArr);
    }
    return propArr;
  }

  parseProperties(data = [], bucket = []) {
    if (data != null && data.length > 0) {
      try {
        for (var i = data.length - 1; i >= 0; i--) {
          Object.keys(data[i].data).forEach((key) => {
            // change for loop?
            this.countExistsObjList(bucket, key);
          });
        }
      } catch (error) {}
    }
    return bucket;
  }

  countExistsObjList(list, name) {
    for (var i = list.length - 1; i >= 0; i--) {
      if (name === list[i].name) {
        list[i].count++;
        return true;
      }
    }
    pushSorted(
      list,
      {
        name: name,
        count: 1,
        status: false,
        rule: this.DEFAULT_REGEX_STRING,
      },
      (e) => {
        try {
          return e.count;
        } catch (error) {}
      }
    );
    return false;
  }

  getDataToPlot(dataset = [], properties = []) {
    let parseData = [];
    let keys = [];
    let rules = [];

    let parseObj = {};
    let parseVal;

    if (dataset.length && properties.length) {
      for (var n = properties.length - 1; n >= 0; n--) {
        keys.push(properties[n].name); //keys = properties.map((elem) => elem.name);
        rules.push(properties[n].rule); //rules = properties.map((elem) => elem.rule);
        // should be a dict with key:rule
      }

      for (var i = dataset.length - 1; i >= 0; i--) {
        let obj = {};

        obj.id = dataset[i].name;

        let data = dataset[i].collect;
        for (var j = data.length - 1; j >= 0; j--) {
          /*for (var z = properties.length - 1; z >= 0; z--) { //OPTION 1
			            try {
			              obj[properties[z].name] = parseFloat(
			                data[j].data[properties[z].name].match(properties[z].rule)[0]
			              ); //should have here an option to aggregation function for each station (inside) data or/and stations data
			            } catch (error) {
			              //console.log("fail");
			            }
		         	}*/

          try {
            // OPTION 2
            let subObj = pick(data[j].data, keys);

            parseObj = {};
            for (let x = 0; x < keys.length; x++) {
              try {
                parseVal = parseFloat(subObj[keys[x]]);
                if (parseVal) parseObj[keys[x]] = parseVal;
              } catch (error) {
                //console.log(error, keys[x]);
              }
            }
            obj = { ...obj, ...parseObj };
          } catch (error) {
            //console.log(error);
          }
        }

        if (obj) {
          parseData.push(obj);
        }
      }
    }

    return { data: parseData, keys: keys };
  }

  /**
   * give the regex as a function that receives a key and returns the regex string
   */
  pickObjRegex = (obj, keys, regex) =>
    keys.reduce((a, b) => {
      try {
        a[b] = parseFloat(obj[b].match(regex)[0]);
      } catch (error) {}

      return a;
    }, {});

  getParsedObj(
    stationData,
    keys,
    obj = {},
    obj_pick = this.pickObjRegex,
    default_regex = this.DEFAULT_REGEX_STRING
  ) {
    // this is the winner if we dont have to search for rules
    // obj_pick(obj, keys, regex) ; see  pickObjRegex
    // stationData = data[j] ; see getDataToPlot()
    try {
      let subObj = obj_pick(stationData.data, keys, default_regex);
      obj = { ...obj, ...subObj };
    } catch (error) {
      //console.log("shit",error)
    }
    return obj;
  }

  getObjSubset(obj, keys) {
    // ex keys = ['keyone', 'keytwo']
    return keys.reduce((a, b) => {
      a[b] = obj[b];
      return a;
    }, {});
  }
}

export default ReadLocal;
