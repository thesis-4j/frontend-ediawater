import React from "react";
import { Pagination } from "react-bootstrap";

export const Paginator = ({ handleSetTab, tabDisplay }) => (
  <Pagination size="sm">
    <Pagination.First />
    <Pagination.Item
      key={"dataviewer"}
      active={tabDisplay === "dataviewer"}
      onClick={() => {
        handleSetTab("dataviewer");
      }}
    >
      Data
    </Pagination.Item>
    <Pagination.Item
      key={"plot"}
      active={tabDisplay === "plot"}
      onClick={() => {
        handleSetTab("plot");
      }}
    >
      Results
    </Pagination.Item>
    <Pagination.Last />
  </Pagination>
);

export default Paginator;
