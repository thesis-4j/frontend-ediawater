import React, { PureComponent } from "react";
import { ResponsiveCirclePackingCanvas } from "@nivo/circle-packing";

export class PlotBubbleCircle extends PureComponent {
  static defaultProps = {
    data: {},
  };

  componentDidUpdate(prevProps, prevState) {
    console.log("lotBubbleCircle updated", this.props.data);
  }

  render() {
    return (
      <div style={{ height: "40rem", width: "60rem", minWidth: 0 }}>
        <ResponsiveCirclePackingCanvas
          data={this.props.data}
          margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
          id="id"
          value="value"
          label={function (e) {
            return e.id + ": " + e.value;
          }}
          colors={{ scheme: "nivo" }}
          colorBy="id"
          childColor={{
            from: "color",
            modifiers: [["brighter", 0.4]],
          }}
          padding={1}
          leavesOnly={false}
          enableLabels={true}
          labelTextColor={{
            from: "color",
            modifiers: [["darker", 2.4]],
          }}
          borderColor={{
            from: "color",
            modifiers: [["darker", 0.3]],
          }}
          animate={true}
        />
      </div>
    );
  }
}

export default PlotBubbleCircle;
