import React, { PureComponent } from "react";

import PlotPointBar from "./PlotPointBar";
import PlotTimeLine from "./PlotTimeLine";
import PlotBubbleCircle from "./PlotBubbleCircle";

export class PlotHOC extends PureComponent {
  componentDidUpdate() {
    console.log("PlotHOC.js props", this.props);
  }

  plotSelection = (plotData) => ({
    point: (
      <PlotPointBar
        data={plotData.data}
        keys={plotData.keys}
        idx={"id"}
        style={{
          overflowX: "scroll",
          overflowY: "scroll",
        }}
      />
    ),
    tseries: (
      <PlotTimeLine
        data={plotData}
        style={{
          overflowX: "scroll",
          overflowY: "scroll",
        }}
      />
    ),

    bubble: (
      <PlotBubbleCircle
        data={plotData}
        style={{
          overflowX: "scroll",
          overflowY: "scroll",
        }}
      />
    ),
  });

  render() {
    return (
      <React.Fragment>
        {this.props.plotData
          ? this.plotSelection(this.props.plotData)[this.props.plotOption]
          : null}
      </React.Fragment>
    );
  }
}

export default PlotHOC;
