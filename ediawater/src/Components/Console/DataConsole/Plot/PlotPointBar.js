import React, { Component } from "react";
import { ResponsiveBarCanvas } from "@nivo/bar";
import { fromJS } from "immutable";

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.

// data = [{key1: something, ...}]
// keys = [key1, ...] from data that will be ploted
// idx = 'key' is the selective identifier that defined who is that data group

const DEFAULT_INDEX = "id";

export class PlotPointBar extends Component {
  static defaultProps = {
    data: [],
    keys: [],
    idx: DEFAULT_INDEX,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      keys: [],
      idx: "id",
      prevKeysImm: [],
      prevDataImm: [],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const immKeys = fromJS(nextProps.keys);
    const immData = fromJS(nextProps.data);

    if (
      !immKeys.equals(prevState.prevKeysImm) ||
      nextProps.data.length !== prevState.data.length ||
      !immData.equals(prevState.prevDataImm) ||
      nextProps.idx !== prevState.idx
    ) {
      return {
        data: nextProps.data,
        keys: nextProps.keys,
        idx: nextProps.idx,
        prevKeysImm: immKeys,
      };
    }
    return null;
  }

  render() {
    return (
      <div style={{ height: "40rem", width: "45rem" }}>
        <ResponsiveBarCanvas
          data={this.state.data}
          keys={this.state.keys}
          indexBy={this.state.idx}
          margin={{ top: 50, right: 60, bottom: 50, left: 80 }}
          padding={0.3}
          groupMode="grouped"
          valueScale={{ type: "linear" }}
          indexScale={{ type: "band", round: true }}
          colors={{ scheme: "paired" }}
          defs={[
            {
              id: "dots",
              type: "patternDots",
              background: "inherit",
              color: "#38bcb2",
              size: 4,
              padding: 1,
              stagger: true,
            },
            {
              id: "lines",
              type: "patternLines",
              background: "inherit",
              color: "#eed312",
              rotation: -45,
              lineWidth: 6,
              spacing: 10,
            },
          ]}
          borderColor={{
            from: "color",
            modifiers: [["darker", 1.6]],
          }}
          axisTop={null}
          axisRight={null}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: this.state.data.length ? "Water quality station" : "",
            legendPosition: "middle",
            legendOffset: 32,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: this.state.data.length ? "Value" : "",

            legendPosition: "middle",
            legendOffset: -40,
          }}
          labelSkipWidth={12}
          labelSkipHeight={12}
          labelTextColor={{
            from: "color",
            modifiers: [["brighter", "0"]],
          }}
          legends={[
            {
              dataFrom: "keys",
              anchor: "bottom-right",
              direction: "column",
              justify: false,
              translateX: 120,
              translateY: 0,
              itemsSpacing: 2,
              itemWidth: 100,
              itemHeight: 20,
              itemDirection: "left-to-right",
              itemOpacity: 0.85,
              symbolSize: 20,
              effects: [
                {
                  on: "hover",
                  style: {
                    itemOpacity: 1,
                  },
                },
              ],
            },
          ]}
          animate={true}
          motionStiffness={90}
          motionDamping={15}
        />
      </div>
    );
  }
}

export default PlotPointBar;
