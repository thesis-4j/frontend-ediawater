import React, { PureComponent } from "react";
import { ResponsiveLineCanvas } from "@nivo/line";


export class PlotTimeLine extends PureComponent {
  static defaultProps = {
    data: [],
  };

  componentDidUpdate(prevProps, prevState) {
    console.log("PlotTimeLine updated", this.props.data);
  }

  render() {
    return (
      <div
        style={{
          height: "40rem",
          width: "100rem",
          minWidth: 0,
          backgroundColor: "rgba(0, 0, 0, 0.01)",
        }}
      >
        <ResponsiveLineCanvas
          data={this.props.data}
          margin={{ top: 50, right: 150, bottom: 50, left: 100 }}
          xScale={{
            type: "point",
          }}
          yScale={{
            type: "linear",
            stacked: false,
            min: "auto",
            max: "auto",
            reverse: false,
          }}
          yFormat=" >-.2f"
          axisTop={null}
          axisRight={null}
          axisBottom={{
            orient: "bottom",
            tickPadding: 5,
            tickRotation: -40,
            legend: this.props.data ? "Time" : "",
            legendOffset: 36,
            legendPosition: "middle",
          }}
          axisLeft={{
            orient: "left",
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: this.props.data.length ? "Value" : "",
            legendOffset: -50,
            legendPosition: "middle",
          }}
          dotSize={10}
          dotColor="inherit:darker(0.3)"
          dotBorderWidth={2}
          dotBorderColor="#ffffff"
          enableDotLabel={true}
          dotLabel="y"
          useMesh={true}
          dotLabelYOffset={-12}
          animate={true}
          motionStiffness={90}
          motionDamping={15}
          legends={[
            {
              anchor: "bottom-right",
              direction: "column",
              justify: false,
              translateX: 100,
              translateY: 0,
              itemsSpacing: 0,
              itemDirection: "left-to-right",
              itemWidth: 80,
              itemHeight: 20,
              itemOpacity: 0.75,
              symbolSize: 12,
              symbolShape: "circle",
              symbolBorderColor: "rgba(0, 0, 0, .5)",
              effects: [
                {
                  on: "hover",
                  style: {
                    itemBackground: "rgba(0, 0, 0, .03)",
                    itemOpacity: 1,
                  },
                },
              ],
            },
          ]}
        />
      </div>
    );
  }
}

export default PlotTimeLine;
