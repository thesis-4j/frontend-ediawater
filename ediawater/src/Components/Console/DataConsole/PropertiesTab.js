import React, { useState } from "react";
import { Badge, Button, FormControl, Spinner } from "react-bootstrap";

export const PropertiesTab = ({
  handlePropViewerInput,
  handleSelectProperty,
  handleClickInput,
  properties,
  filterProperty,
  loadingState,
}) => {
  console.log("received properties", properties.length);
  return (
    <>
      <PropViewer
        handleInput={handlePropViewerInput}
        onClickInput={handleClickInput}
        loadingState={loadingState}
      />

      <div className={"PropertyBadge"}>
        {properties
          .filter(
            (e) =>
              e.name.toLowerCase().indexOf(filterProperty.toLowerCase()) !== -1
          )
          .map((elem, id) => (
            <Button
              key={id}
              variant="secondary"
              size={"sm"}
              active={elem.status}
              onClick={handleSelectProperty.bind(this, elem)}
            >
              <Badge variant="light">{elem.count}</Badge> {elem.name}
            </Button>
          ))}
      </div>
    </>
  );
};

const PropViewer = ({ handleInput, onClickInput, loadingState }) => {
  let [control, setHide] = useState(false);

  return (
    <div>
      <h6>
        Properties
        <Button
          size="sm"
          style={{ width: "1.5rem", height: "1.5rem", margin: "1%" }}
          variant="info"
          className={"RegexButtonInput"}
          onClick={() => {
            setHide(!control);
            if (onClickInput !== undefined) onClickInput();
          }}
        ></Button>
        {loadingState && (
          <Spinner
            as="span"
            animation="grow"
            variant="primary"
            size="sm"
            role="status"
            aria-hidden="true"
          />
        )}
      </h6>

      {control && (
        <div className={"InputGroupDrop"}>
          <FormControl
            placeholder="Search"
            aria-describedby="basic-addon1"
            style={{ height: "1.5rem", margin: "1%" }}
            onChange={handleInput}
          />
        </div>
      )}
    </div>
  );
};

const PropertiesTabMemo = React.memo(PropertiesTab);

export default PropertiesTabMemo;
