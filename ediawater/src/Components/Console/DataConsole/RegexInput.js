import React, { useState, useLayoutEffect } from "react";
import {
  Form,
  Button,
  ButtonGroup,
  DropdownButton,
  Dropdown,
  FormControl,
} from "react-bootstrap";

import { ExistsInList } from "../../../Libraries/UtilsLib";

const DEFAULT_REGEX_STRING = "[0-9]+[.,]?[0-9]*"; //str.match(DEFAULT_REGEX_STRING)

export const RegexInput = ({ properties, propertiesRegex }) => {
  let [inputProperty, setFilterProperty] = useState("");
  let [regexProperty, setRegexPropertyButton] = useState(null);
  let [inputRegex, setRegexInput] = useState(DEFAULT_REGEX_STRING);
  let [rules, setRule] = useState({});

  const handleUpdateRule = (name, rule) => {
    propertiesRegex[name] = rule;
    setRule({ ...rules, [name]: rule });
  };

  const handleRemoveRule = (elem) => {
    let status = delete propertiesRegex[elem];
    if (status) {
      setRule(propertiesRegex);
    }
  };

  useLayoutEffect(() => {
    //check if exists, otherwise remove rules

    let rulesCopy = { ...rules };
    let rulesKeys = Object.keys(rules);
    let updateFlag = false;

    for (var i = rulesKeys.length - 1; i >= 0; i--) {
      if (
        !ExistsInList(
          properties,
          rulesKeys[i],
          (obj, target) => obj.name === target
        )
      ) {
        delete rulesCopy[rulesKeys[i]];
        updateFlag = true;
      }
    }
    if (updateFlag) {
      console.log("Regex useLayoutEffect - rules updated", rulesCopy, rules);
      setRule({ ...rulesCopy });
    }
    setRegexPropertyButton(null);
  }, [properties, rules]);

  return (
    <>
      <div>
        <h6>Regex rules</h6>
      </div>

      <div className={"RegexInput"}>
        <Form.Control
          id="Regex input"
          size="sm"
          placeholder="(Example) /([A-Z])\w+/g"
          onChange={(e) => {
            setRegexInput(e.target.value);
          }}
          defaultValue={DEFAULT_REGEX_STRING}
          style={{ width: "45%", height: "35px" }}
        />
        <DropdownButton
          as={ButtonGroup}
          id={"dropdown-regex"}
          variant={"warning"}
          title={
            regexProperty == null ? "Property" : regexProperty.slice(0, 15)
          }
          style={{ maxHeight: "35px" }}
        >
          <div className={"InputGroupDrop"}>
            <FormControl
              placeholder="Search"
              aria-describedby="basic-addon1"
              style={{ height: "1.5rem", margin: "1%" }}
              onChange={(e) => {
                setFilterProperty(e.target.value);
              }}
            />
          </div>
          <Dropdown.Divider />
          {properties
            .filter(
              (e) =>
                e.name.toLowerCase().indexOf(inputProperty.toLowerCase()) !== -1
            )
            .map((elem, i) => (
              <Dropdown.Item
                key={i}
                onClick={() => setRegexPropertyButton(elem.name)}
              >
                {elem.name}
              </Dropdown.Item>
            ))}
        </DropdownButton>

        <Button
          variant="info"
          size={"sm"}
          className={"RegexAddButton"}
          onClick={() => handleUpdateRule(regexProperty, inputRegex)}
          style={{ borderRadius: "50%" }}
        ></Button>
      </div>
      <div className={"ShowRegexRules"}>
        {Object.keys(rules).map((elem, i) => (
          <div className={"RegexInput"} key={i}>
            <Form.Control
              size="sm"
              placeholder={rules[elem]}
              text={rules[elem]}
              readOnly
              style={{ height: "35px", margin: "1%" }}
            />
            <Button variant="warning" style={{ height: "35px" }}>
              {elem.slice(0, 10)}..
            </Button>
            <Button
              variant="danger"
              size={"sm"}
              style={{ height: "35px", margin: "1%" }}
              onClick={() => handleRemoveRule(elem)}
            >
              Remove
            </Button>
          </div>
        ))}
      </div>
    </>
  );
};

const RegexInputMemo = React.memo(RegexInput);

export default RegexInputMemo;
