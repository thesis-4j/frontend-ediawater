/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo receive data via props handler
 * @todo
 */

import React, { Component } from "react";
import { Image, Form, Button, Dropdown, ButtonGroup } from "react-bootstrap";
import PropTypes from "prop-types";

import { images } from "../../assets/Images";

// Constants
let API_SERVER_URL = null; // API base url
const TIMEOUT = 180000; // 180s timeout

const endpointApi = {
  waternodes: "upload/csv/waternodes",
  links: "upload/csv/link/waternodes",
  qualitystations: "upload/csv/water-quality/stations",
  qualitydata: "upload/csv/water-quality/data",
  gis: "upload/map",
};

function formRender(descriptionRef, categoryRef) {
  return (
    <Form.Group className={"FileImportInput"}>
      <Form.Control
        id="description"
        size="sm"
        ref={descriptionRef}
        placeholder="Description"
      />
      <Form.Control as="select" size="sm" ref={categoryRef} custom>
        <option value="watershed">Watershed</option>
        <option value="land-use">Land Use</option>
        <option value="geometry">Geometry</option>
      </Form.Control>
    </Form.Group>
  );
}

export class FileImport extends Component {
  static propTypes = {
    accept: PropTypes.string,
  };

  static defaultProps = {
    accept: ".shp, .csv, .json, .geojson, .zip",
  };

  constructor(props) {
    super(props);
    this.state = {
      accept: this.props.accept,
      input: {},
      category: " ",
      description: " ",
      destpath: "upload/file",
      option: null,
      uploadIcon: null,
      uploadText: "",
    };

    this.descriptionRef = React.createRef();
    this.categoryRef = React.createRef();
    this.renderOptions = {
      gis: formRender(this.descriptionRef, this.categoryRef),
      qualitydata: null,
      links: null,
      qualitystations: null,
    };

    API_SERVER_URL = process.env.REACT_APP_API_HOST
      ? process.env.REACT_APP_API_HOST
      : "http://localhost:5001/";
  }

  /**
  Save the input file as a state
    @param {file} input -  can be multi-file
  */
  fileHandler = (input) => {
    this.setState({ input: input });
  };

  /**
  Add extension name to url endpoint  based on event key (dropdown)
    @param {string} destinput -
  */
  addExtension = (destinput) => {
    this.setState({
      destpath: endpointApi[destinput],
      option: destinput,
      uploadIcon: null,
      uploadText: "#" + destinput,
    });
  };

  /**
  used as content info for the API endpoint - saves all file dicts.
  The data wil be a list of dict that will identify all files inside FormData
  */
  createDataBlob = () => ({ data: [], storage: "neo4j-driver" });

  /**
  returns a dict used to describe the file
    @param {string} name - name of the file
    @param {string} description - description about of the file's content
    @param {string} category - file sub-type/category
  */
  createFileHeader = (name, description = " ", category = " ") => ({
    name: String(name),
    description: String(description),
    category: category,
  });

  /**
  Handles the saved fiels at component's state and sends data via uploadNeo4jAPI
  Will send a multiform type data as: {[JSON][file1][file2]..}
    @param {object} event -
  */

  handleOnSubmit = (event) => {
    event.preventDefault();

    if (this.state.input && Object.keys(this.state.input).length === 0) {
      alert("No file uploaded");
      return;
    }

    if (this.state.destpath === null) {
      alert("No option selected");
      return;
    }

    let description = this.state.description;
    let category = this.state.category;

    if (
      this.state.option === "gis" &&
      this.descriptionRef.current.value != null
    ) {
      description = this.descriptionRef.current.value;
      category = this.categoryRef.current.value;
    }
    const { input } = this.state;
    const formData = new FormData();
    const info = this.createDataBlob();

    //  Here we save all info about of each file inside of a DataBlob
    for (let i = 0; i < input.files.length; i++) {
      console.log(info, input.files[i].name, input.files.length);
      formData.append(String(input.files[i].name), input.files[i]);
      info.data.push(
        this.createFileHeader(input.files[i].name, description, category)
      );
    }

    formData.append("JSON", JSON.stringify(info));

    //  console.log("Check Processed Data:");
    //  use values this way (entries) - convert this into utils library
    //  for (var key of formData.entries()) {
    //    console.log(`${key[0]}, ${key[1]}`);
    //  }

    try {
      this.uploadNeo4jAPI(formData);
    } catch (error) {
      console.log("error");
    }
  };

  uploadNeo4jAPI = async (formData) => {
    const controller = new AbortController();
    const timerFetch = setTimeout(() => controller.abort(), TIMEOUT); // timeout
    this.setState({ uploadIcon: images.File.up });

    fetch(API_SERVER_URL + this.state.destpath, {
      method: "post",
      mode: "cors",
      body: formData,
      signal: controller.signal,
      config: {
        headers: { "Content-Type": "multipart/form-data" },
      },
    })
      .then((res) => {
        if (res.ok) {
          this.setState({ uploadIcon: images.File.ok });
          // alert("File uploaded successfully.");
        } else {
          if (res.status === 406) {
            alert("The file is not valid");
          } else if (res.status >= 500) {
            alert("The server cannot handle with the request's content");
          }
          this.setState({
            uploadIcon: images.File.cancel,
            uploadText: " ",
            destpath: "upload/file",
          });
        }
      })
      .catch((error) => {
        if (error.name === "AbortError") {
          alert("Response timed out");
        } else {
          console.error("Uh oh, an error!", error);
        }
      })
      .finally(() => {
        clearTimeout(timerFetch);
        this.setState({ destpath: "upload/file", uploadText: " " });
      });
  };

  render() {
    return (
      <div className={"FileSection"}>
        <div>
          <Form onSubmit={this.handleOnSubmit.bind(this)} role="form">
            <input
              label="Custom file input"
              accept={this.state.accept}
              type="file"
              id="input"
              size="sm"
              multiple
              onChange={(event) => this.fileHandler(event.target)}
            />

            <Dropdown as={ButtonGroup}>
              <Button
                variant="primary"
                id="dropdown-import-button"
                onClick={this.handleOnSubmit}
                size="sm"
              >
                UPLOAD{" "}
                <span style={{ fontWeight: "bold" }}>
                  {this.state.uploadText}
                </span>
              </Button>
              <Dropdown.Toggle
                split
                variant="primary"
                id="dropdown-import-list"
              />
              <Image
                src={this.state.uploadIcon}
                className={"IconAfterUpload"}
              />
              <Dropdown.Menu className="dropdown-menu">
                <Dropdown.Item
                  eventKey="qualitystations"
                  onSelect={() => this.addExtension("qualitystations")}
                >
                  Water Quality Stations
                </Dropdown.Item>
                <Dropdown.Item
                  eventKey="qualitydata"
                  onSelect={() => this.addExtension("qualitydata")}
                >
                  Water Quality Data
                </Dropdown.Item>
                <Dropdown.Item
                  eventKey="waternodes"
                  onSelect={() => this.addExtension("waternodes")}
                >
                  Water Nodes
                </Dropdown.Item>
                <Dropdown.Item
                  eventKey="links"
                  onSelect={() => this.addExtension("links")}
                >
                  Nodes Relationships
                </Dropdown.Item>

                <Dropdown.Item
                  eventKey="gis"
                  onSelect={() => this.addExtension("gis")}
                >
                  Shapefile
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <div>{this.renderOptions[this.state.option]}</div>
          </Form>
        </div>
      </div>
    );
  }
}
