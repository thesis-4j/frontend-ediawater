/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo
 * @todo
 */

import React, { Component } from "react";
import PropTypes from "prop-types";
import GoogleMapReact from "google-map-react";

import Marker from "./Markers";
import SimpleMarker from "./SimpleMarker";
import MapDropdownMode from "./MapDropdownMode";
import MapDropdownChunk from "./MapDropdownChunk";

import { publish, DataTopic } from "../../../PubSub/PubSub";
import { sliceChunk } from "../../../Libraries/UtilsLib";

//import { geojsonObject } from "../../Libraries/Geojson"; //debug

// Functions
/* function fpolylineHandler (event, elem) {
  console.log('handler', event, elem)
} */

const htmlInfoWindow = (info) =>
  "<div style='color: black' align='left' >" +
  "<p>Type: " +
  info.type +
  "</p>" +
  "<p>Sub-Type: " +
  info.subtype +
  "</p>" +
  "<p>Flow: " +
  info.flow +
  "</p>" +
  "<p>Start: " +
  info.start +
  "</p>" +
  "<p>End: " +
  info.end +
  "</p>" +
  "</div>" +
  "<div>" +
  "<button id='polyButton'>Hide</button>" +
  "</div>";

const AnyReactComponent = ({ text }) => (
  <div className={"MarkersZPoint"}>{text}</div>
);

// Constants
// const containerStyle = { height: "400vh", width: "200%" };
const markers = [
  { lat: 37.772, lng: -12.214 },
  { lat: 21.291, lng: -157.821 },
  { lat: -18.142, lng: 178.431 },
  { lat: -27.467, lng: 153.027 },
];

const heat = {
  positions: markers,
  options: {
    radius: 20,
    opacity: 0.6,
  },
};

export class Map extends Component {
  static propTypes = {
    center: PropTypes.object,
    zoom: PropTypes.number,
    relations: PropTypes.array,
    infra: PropTypes.array,
  };

  static defaultProps = {
    center: {
      lat: 38.26,
      lng: -7.61,
    },
    zoom: 11,
  };

  constructor(props) {
    super(props);

    this.state = {
      infra: [],
      infraHidden: [],
      infraStyle: {}, //infraStyle
      relations: [],
      qualityStation: [],
      qualityStationConf: {},
      qualityStationGroup: [[]],
      qualityStationGroupSelected: [],
      shapefile: [],
      shapefileConf: {},
      mapFeatures: { geojson: [] },
      mapLayer: "roadmap",
      center: {},
      mapMode: "view",
      queryInfraLink: [],
      queryInfraLinkRule: null,
      queryInfraMarker: [],
      queryInfraMarkerRule: null,
    };

    this.mapRef = React.createRef();
    this.gMap = {
      map: null,
      polylines: [],
      window: {
        nortEast: { lat: 38.451473, lng: -7.318052 },
        southWest: { lat: 38.018586, lng: -7.85653 },
      },
    };

    this.queryInfraLinkBucket = { polylines: [] };

    this.fpolylineHandler = this.fpolylineHandler.bind(this);
    this.getClickReturnHideInfra = this.getClickReturnHideInfra.bind(this);
    this.getClickStation = this.getClickStation.bind(this);
    this.handleMapMode = this.handleMapMode.bind(this);
    this.handleChunkData = this.handleChunkData.bind(this);
    this.getQueryPathResult = this.getQueryPathResult.bind(this);
    this.getQueryPointResult = this.getQueryPointResult.bind(this);
    this.renderQueryPoly = this.renderQueryPoly.bind(this);
    this.changeGeoJSONVisibility = this.changeGeoJSONVisibility.bind(this);
    this.clearTempBuckerQuery = this.clearTempBuckerQuery.bind(this);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // console.log("nextprops", nextProps);
    if (typeof nextProps !== "undefined") {
      let qualityStations = nextProps.qualityStation;
      const initialDataIndex = 0;

      let qualityStationGp = [];
      if (qualityStations.length) {
        qualityStationGp = sliceChunk(qualityStations, 700);
        qualityStations = qualityStationGp[initialDataIndex];
      }

      // const relations = this.createPath(nextProps.relations)
      this.setState({
        infra: nextProps.infra,
        relations: nextProps.relations,
        infraStyle: nextProps.infraConf,
        shapefile: nextProps.shapefile,
        shapefileConf: nextProps.shapefileConf,
        qualityStation: qualityStations,
        qualityStationConf: nextProps.qualityStationConf,
        qualityStationGroup: qualityStationGp,
        mapLayer: nextProps.mapLayer,
        windowFilter: nextProps.windowFilter,
        qualityStationGroupSelected: [initialDataIndex],
      });
    }
  }

  componentDidMount(nextProps) {
    // console.log("Map.js Did mounted");
  }

  componentDidUpdate(prevProps) {
    console.log("Map.js Did update");
    if (this.gMap.map !== null && this.mapRef !== null) {
      let { map_, maps_ } = this.gMap.map;
      //let { map_, maps_ } = this.mapRef;
      if (map_ !== null || maps_ !== null) {
        this.renderPolylines(map_, maps_, this.state.relations, this.gMap);

        if (
          prevProps.shapefileConf.hidden !== this.props.shapefileConf.hidden ||
          prevProps.shapefile.length !== this.props.shapefile.length
        ) {
          this.changeGeoJSONVisibility(
            this.state.shapefileConf.hidden,
            map_,
            maps_
          );
        }
        this.changePolylineVisibility(this.props.relations, map_); // a variable, instaed  updating  entire array

        this.renderQueryPoly(
          map_,
          maps_,
          this.state.queryInfraLink,
          this.state.queryInfraLinkRule,
          "#0f03fc",
          5
        );

        // console.log('setmap', this.state.mapLayer, this.props.mapLayer)
        map_.setMapTypeId(this.state.mapLayer); // from here create a function mapWindowInfo()

        this.getWindowBounds(map_);
      }
    }
  }

  handleMapMode(mode) {
    console.log("Set mode", mode);

    if (mode === "reset") {
      this.clearTempBuckerQuery();
      this.setState({ queryInfraLink: [], queryInfraMarker: [] });
    } else {
      this.setState({ mapMode: mode });
    }
  }

  getWindowBounds(map) {
    const bounds = map.getBounds();
    const nortEast = bounds.getNorthEast();
    const southWest = bounds.getSouthWest();

    try {
      this.gMap.window.nortEast = { lat: nortEast.lat(), lng: nortEast.lng() };
      this.gMap.window.southWest = {
        lat: southWest.lat(),
        lng: southWest.lng(),
      };
    } catch (error) {
      console.log("Map Not loaded yet", error);
    }
  }

  changeGeoJSONVisibility(key, map, maps) {
    if (key) {
      this.removeGeoJson(map, maps);
    } else {
      this.renderGeoJson(map, maps);
    }
  }

  changePolylineVisibility(data, map) {
    const status = data.length > 0;

    if (status === false) {
      for (let i = 0; i < this.gMap.polylines.length; i++) {
        this.gMap.polylines[i].setMap(null);
        this.gMap.polylines[i].visible = status;
      }
    } else {
      for (let i = 0; i < this.gMap.polylines.length; i++) {
        this.gMap.polylines[i].setMap(map);
        this.gMap.polylines[i].visible = status;
      }
    }
  }

  fpolylineHandler(event, elem) {
    this.gMap.polylines[elem].setMap(null); // handleHidePolyline
  }

  /**
  Parse endpoint data from `api/water/relation`
    @param {array} endpointdata - api/water/relation
    @return {array} - List of list, where each list represents a path
  */
  createPath(endpointdata = []) {
    return endpointdata.map((elem) => [elem.startcoord, elem.endcoord]);
  }

  infoPolyline(Polyline, map, maps, idx, event) {
    const infowindow = new maps.InfoWindow({
      content: " ",
    });
    infowindow.setContent(htmlInfoWindow(this.state.relations[idx]));

    maps.event.addListener(infowindow, "domready", () => {
      document.getElementById("polyButton").onclick = (e) =>
        this.fpolylineHandler(e, idx);
    });

    infowindow.setPosition(event.latLng);
    infowindow.open(map, Polyline);
  }

  /**
  Renders non geodesic polyline (straight line)
    @param {Object} map - map object
    @param {Object} maps - google.maps object
    @param {array} path - array that contains two objects `[{lat;,lng;},{lat;,lng;}]`
  */
  polylinesConstructor(
    map,
    maps,
    path,
    bucket,
    handlerEvent = null,
    color = "#192734",
    thickness = 3
  ) {
    //console.log("received path", path);
    const nonGeodesicPolyline = new maps.Polyline({
      path: path,
      geodesic: false,
      strokeColor: color,
      strokeOpacity: 1,
      strokeWeight: thickness,
      icons: [
        {
          // arrow polyline
          icon: { path: maps.SymbolPath.FORWARD_CLOSED_ARROW },
          offset: "100%",
        },
      ],
    });
    nonGeodesicPolyline.setMap(map);
    const idx = bucket.polylines.push(nonGeodesicPolyline);

    // Add a listener for the click event
    if (handlerEvent) {
      maps.event.addListener(
        nonGeodesicPolyline,
        "click",
        handlerEvent.bind(this, nonGeodesicPolyline, map, maps, idx - 1)
      );
    }
  }

  /**
  Allow to render multipath Polyline, uses the state.relations as multipah source
    @param {Object} map - map object
    @param {Object} maps - google.maps object
  */
  renderPolylines(map, maps, multipath, pathbucket = { polylines: [] }) {
    if (pathbucket.polylines.length === 0) {
      try {
        multipath.forEach((p) => {
          this.polylinesConstructor(
            map,
            maps,
            [p.startcoord, p.endcoord],
            pathbucket,
            this.infoPolyline
          );
        });
      } catch (error) {
        console.log("Not loaded yet to render Polylines");
      }
    }

    return pathbucket;
  }

  getRandomColor() {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16);

    return "#" + randomColor;
  }

  clearTempBuckerQuery() {
    if (this.queryInfraLinkBucket.polylines.length) {
      this.queryInfraLinkBucket.polylines.forEach((p) => p.setMap(null));
      this.queryInfraLinkBucket = { polylines: [] };
    }
  }

  renderQueryPoly(map, maps, data, parseRule, color = "#0f03fc", trace = 5) {
    try {
      //check temporary bucket
      this.clearTempBuckerQuery();

      //console.log("data received", data)
      for (var i = 0; i <= data.length - 1; i++) {
        data[i].forEach((elem) => {
          let path = parseRule(elem);
          //console.log("After parseRule", elem)
          if (path) {
            this.polylinesConstructor(
              map,
              maps,
              path,
              this.queryInfraLinkBucket,
              null,
              this.getRandomColor(),
              trace
            );
          }
        });
      }
    } catch (error) {
      console.log(error, "Not loaded yet to render Polylines");
    }
  }

  getQueryPathResult(path, rule) {
    this.setState({ queryInfraLink: path, queryInfraLinkRule: rule });
  }

  getQueryPointResult(points, rule) {
    this.setState({ queryInfraMarker: points, queryInfraMarkerRule: rule });
  }

  removeGeoJson(map, maps) {
    //console.log(map, maps,this.state.mapFeatures.geojson);
    for (let i = 0; i < this.state.mapFeatures.geojson.length; i++) {
      for (let j = 0; j < this.state.mapFeatures.geojson[i].length; ++j) {
        //console.log("render", this.state.mapFeatures.geojson[i][j]); // remove from  using closure_uid_247428810 seems that does not work
        map.data.remove(this.state.mapFeatures.geojson[i][j]);
      }
    }
    this.setState({ mapFeatures: { geojson: [] } });
  }

  renderGeoJson(map, maps) {
    /* map.data.loadGeoJson( "https://storage.googleapis.com/mapsdevsite/json/google.json"); // lod from remote */
    // console.log(geojsonObject);
    /*var features = map.data.addGeoJson(geojsonObject);
     this.state.mapFeatures.geojson.push(features)
      */

    console.log(this.state.mapFeatures.geojson);
    for (let i = 0; i < this.state.shapefile.length; i++) {
      const geojsonData = map.data.addGeoJson(this.state.shapefile[i].geojson);
      //console.log(geojsonData);
      this.state.mapFeatures.geojson.push(geojsonData);
    }
  }

  getClickReturnHideInfra(key) {
    const newInfraHidden = this.state.infra.splice(key, 1);
    this.setState({ infraHidden: newInfraHidden });
  }

  whenMapLoaded(gMapObj) {
    const { map, maps } = gMapObj;
    this.gMap.map = gMapObj;
    this.renderPolylines(map, maps, this.state.relations, this.gMap);
  }

  getClickStation(key) {
    console.log("Receive station return", key);
    publish(DataTopic, [this.state.qualityStation[key]]);
  }

  handleChunkData(event, index) {
    console.log("handleChunkData:", index);

    if (event.target.checked) {
      let newData = [];
      let newIndexes = [...this.state.qualityStationGroupSelected, index];
      console.log("newIndexes", newIndexes);

      newIndexes.forEach((i) => {
        console.log("index: ", i);
        newData = [...newData, ...this.state.qualityStationGroup[i]];
      });

      this.setState({
        qualityStationGroupSelected: newIndexes,
        qualityStation: newData,
      });
    } else {
      let newData = [];
      let indexes = this.state.qualityStationGroupSelected;
      //console.log("before", indexes.length, indexes);
      indexes.splice(index, 1);
      //console.log("after", indexes.length, indexes);
      indexes.forEach((i) => {
        //console.log("index: ", i);
        newData = [...newData, ...this.state.qualityStationGroup[i]];
      });

      this.setState({
        qualityStationGroupSelected: indexes,
        qualityStation: newData,
      });
    }
  }

  insideMapBounds(window, marker) {
    try {
      return (
        marker.latitude < window.nortEast.lat &&
        marker.latitude > window.southWest.lat &&
        marker.longitude < window.nortEast.lng &&
        marker.longitude > window.southWest.lng
      );
    } catch (error) {
      return true;
    }
  }

  selectInsideWindowMarker = () =>
    this.state.qualityStation.reduce((acc, station, i) => {
      if (this.insideMapBounds(this.gMap.window, station)) {
        try {
          acc.push(
            <SimpleMarker
              lat={station.latitude}
              lng={station.longitude}
              type="Water Quality station"
              name={station.id}
              idx={i}
              key={i}
              conf={this.state.qualityStationConf}
              color={this.state.qualityStationConf.color}
              icon={this.state.qualityStationConf.icon}
              returnClick={this.getClickStation}
            />
          );
        } catch (error) {}
      }
      return acc;
    }, []);

  render() {
    //console.log("test", this.state.qualityStationGroup.length && true);
    return (
      <div style={{ height: "400vh", width: "200%" }}>
        <span className={"MapOverlap"}>
          <MapDropdownMode handleMapMode={this.handleMapMode} />
          <MapDropdownChunk
            dataGroup={this.state.qualityStationGroup}
            checked={this.state.qualityStationGroupSelected}
            handleCheck={this.handleChunkData}
          />
        </span>

        <GoogleMapReact
          bootstrapURLKeys={{
            key: "AIzaSyDxcRGVbp1LUdTL60SpyYz12KJx0ccjTDE",
            libraries: ["visualization"],
          }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          center={this.props.center}
          heatmap={heat}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={(mapRef) => this.whenMapLoaded(mapRef)}
          ref={(mapRef) => (this.gMap.map = mapRef)}
        >
          {this.state.queryInfraMarker.map((elem, i) => {
            let coord = this.state.queryInfraMarkerRule(elem);
            //console.log("query Marker", coord);
            return (
              <AnyReactComponent
                lat={coord.lat}
                lng={coord.lng}
                key={i}
                name={"RESULT"}
              />
            );
          })}

          {this.state.infra.map(
            ({ location, type, name, id, watersystem }, i) => {
              try {
                return (
                  <Marker
                    lat={location[1]}
                    lng={location[0]}
                    type={type}
                    name={name}
                    group={watersystem}
                    idBD={id}
                    idx={i}
                    key={i}
                    returnClick={this.getClickReturnHideInfra}
                    returnPathQuery={this.getQueryPathResult}
                    returnPointQuery={this.getQueryPointResult}
                    icon={this.state.infraStyle.icon}
                    label={this.state.infraStyle.label}
                  />
                );
              } catch (error) {
                console.log("ERROR FOUND", type, name, location);
                return null;
              }
            }
          )}

          {this.state.windowFilter
            ? this.selectInsideWindowMarker(true)
            : this.state.qualityStation.map((station, i) => {
                try {
                  return (
                    <SimpleMarker
                      lat={station.latitude}
                      lng={station.longitude}
                      type="Water Quality station"
                      name={station.id}
                      idx={i}
                      key={i}
                      returnClick={this.getClickStation}
                      conf={this.state.qualityStationConf}
                      color={this.state.qualityStationConf.color}
                      icon={this.state.qualityStationConf.icon}
                    />
                  );
                } catch (error) {
                  console.log("ERROR FOUND", station);
                  return null;
                }
              })}
        </GoogleMapReact>
      </div>
    );
  }
}

export default Map;
