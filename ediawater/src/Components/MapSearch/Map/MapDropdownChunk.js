import React, { PureComponent } from "react";
import { Form, DropdownButton } from "react-bootstrap";

//should receive  optional argument for which checkbox will be checked
export class MapDropdownChunk extends PureComponent {
  static defaultProps = {
    checked: [],
  };

  render() {
    return (
      <>
        {
          <DropdownButton
            id="map-button-chunk-overlay"
            variant="link"
            title={"data"}
            size="sm"
            className="OverlayCheckButton"
          >
            <div className="OverlayCheckButtonGroup">
              {this.props.dataGroup.map((elem, i) => (
                <Form.Check
                  inline
                  defaultChecked={i in this.props.checked}
                  key={i}
                  label={`Group ${i}`}
                  type="checkbox"
                  id={`set-data-group-${i}`}
                  onChange={(event) => this.props.handleCheck(event, i)}
                  size="sm"
                />
              ))}
            </div>
          </DropdownButton>
        }
      </>
    );
  }
}

export default MapDropdownChunk;
