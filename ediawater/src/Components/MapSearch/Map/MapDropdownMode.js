import React, { PureComponent } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";

export class MapDropdownMode extends PureComponent {
  render() {
    return (
      <span>
        <DropdownButton
          id="map-button-overlay"
          variant="link"
          title={"mode"}
          size="sm"
          className="OverlayButton"
        >
          <Dropdown.Item
            as="button"
            onSelect={() => this.props.handleMapMode("view")}
          >
            View
          </Dropdown.Item>
          <Dropdown.Item
            as="button"
            onSelect={() => this.props.handleMapMode("select")}
          >
            Select
          </Dropdown.Item>
          <Dropdown.Item
            as="button"
            onSelect={() => this.props.handleMapMode("reset")}
          >
            Clean
          </Dropdown.Item>
        </DropdownButton>
      </span>
    );
  }
}

export default MapDropdownMode;
