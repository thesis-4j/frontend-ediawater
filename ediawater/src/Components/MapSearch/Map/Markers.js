/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo use map.addListener to send info about position of click and chef if it was outside se example https://github.com/cedricdelpoux/react-google-map
 * @todo

 */

/** jshint {inline configuration here} */
import React, { useState } from "react";
import { useLayer } from "react-laag";

import axios from "axios";

import { Image, Button } from "react-bootstrap";
import { images } from "../../../assets/Images";
import { API_SERVER_URL } from "../../../App";

// Constants

const Marker = ({
  idx,
  name,
  type,
  group,
  idBD,
  lat,
  lng,
  $hover,
  returnClick,
  returnPointQuery,
  returnPathQuery,
  icon = false,
  label = true,
  color = "red",
}) => {
  const [isOpen, setOpen] = useState(false);

  const { triggerProps, layerProps, renderLayer } = useLayer({
    isOpen,
    triggerOffset: 10,
    auto: true,
    overflowContainer: false,
    onOutsideClick: () => setOpen(false),
  });

  const handleClick = (prev) => {
    setOpen((prev) => !prev);
  };

  const handleHideButton = () => {
    returnClick(idx);
  };

  const handleShowNearby = () => {
    // [0][nearby][i]["from"]["location"]

    axios.get(API_SERVER_URL + "api/path/nearby/" + idBD).then((res) => {
      const { data } = res;

      console.log("data neaaby", data);
      if (typeof data !== "undefined" && data.length) {
        returnPathQuery([data[0].nearby], (data) => {
          if (data.from != null && data.to != null) {
            return [
              { lat: data.from.location[1], lng: data.from.location[0] },
              { lat: data.to.location[1], lng: data.to.location[0] },
            ];
          }
        });
      }
    });
  };

  const handleForwardPath = () => {
    // for now is foward path ->> should use here apoc.path.spanningTree
    // [0]{ path: [ [location[3], , location[3]]  ]} ->  [{lat;,lng;},{lat;,lng;}]
    axios.get(API_SERVER_URL + "api/path/forward/" + idBD).then((res) => {
      const { data } = res;
      //console.log("foward path", data);
      if (typeof data !== "undefined" && data.length) {
        let fpath = [];
        const datap = data[0].path;
        //console.log("datap", datap.length);

        datap.forEach((elem) => {
          let subpath = [];
          //console.log("elem", elem, elem.length);
          for (var j = 0; j <= elem.length - 3; j += 2) {
            //console.log("elem[j]", elem[j]);
            subpath.push([
              {
                lat: elem[j].location[1],
                lng: elem[j].location[0],
              },
              {
                lat: elem[j + 2].location[1],
                lng: elem[j + 2].location[0],
              },
            ]);
          }
          fpath.push(subpath); //here we should remove the same subpaths
        });

        //console.log("final path", fpath);
        returnPathQuery(fpath, (data) => {
          if (data) {
            return data;
          }
        });
      }
    });
  };

  const handleShowEnd = () => {
    //[0][i].end

    axios.get(API_SERVER_URL + "api/waternode/end/" + idBD).then((res) => {
      const { data } = res;
      console.log("end path", data);
      if (typeof data !== "undefined" && data.length) {
        returnPointQuery(data, (data) => ({
          lat: data.end.location[1],
          lng: data.end.location[0],
        }));
      }
    });
  };

  const handleShowSource = () => {
    //[0][source]

    axios.get(API_SERVER_URL + "api/waternode/source/" + idBD).then((res) => {
      const { data } = res;
      console.log("source path", data);
      if (typeof data !== "undefined" && data.length) {
        returnPointQuery(data, (data) => ({
          lat: data.source.location[1],
          lng: data.source.location[0],
        }));
      }
    });
  };

  const imageProvider = (type, hover) => {
    if (images[type]) {
      if (hover) return images[type].hover;
      return images[type].src;
    }
    return null;
  };

  return (
    <div>
      <div
        className={
          !icon ? ($hover ? "circle hover" : "circle") : "circleInvisible"
        }
        {...triggerProps}
        onClick={(prev) => handleClick(prev)}
      >
        {icon && (
          <Image className="MarkerImage" src={imageProvider(type, $hover)} />
        )}
        {label && (
          <span className="circleText" title={type}>
            {name} {type}
          </span>
        )}
      </div>

      {isOpen &&
        renderLayer(
          <div className="InfoBox" {...layerProps}>
            <div>
              <div>
                <Button size="sm" variant="light" onClick={handleShowNearby}>
                  Show nearby
                </Button>
              </div>
              <div>
                <Button size="sm" variant="light" onClick={handleForwardPath}>
                  Show forward routes
                </Button>
              </div>
              <div>
                <Button size="sm" variant="light" onClick={handleShowSource}>
                  Show source
                </Button>
              </div>
              <div>
                <Button size="sm" variant="light" onClick={handleShowEnd}>
                  Show end node
                </Button>
              </div>
              <div>
                <Button size="sm" variant="light" onClick={handleHideButton}>
                  Hide node
                </Button>
                <div style={{ color: "black", align: "left", fontSize: "80%" }}>
                  <br></br>
                  Name: {name} <br></br>
                  Type: {type} <br></br>
                  System: {group} <br></br>
                  {lat},{lng}
                </div>
              </div>
            </div>
          </div>
        )}
    </div>
  );
};

export default Marker;
