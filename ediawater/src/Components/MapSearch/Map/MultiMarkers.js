/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo use map.addListener to send info about position of click and chef if it was outside se example https://github.com/cedricdelpoux/react-google-map
 * @todo

 */

/** jshint {inline configuration here} */
import React, { PureComponent } from "react";
import SimpleMarker from "./SimpleMarker";

export class MultiMarkers extends PureComponent {
  static defaultProps = {
    size: 2000,
    data: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      qualityStation: this.props.qualityStation,
      size: this.props.size, // default props value 2000
      mapWindow: this.props.mapWindow, // {nortEast:{lat: , lng:} ,  southWest:{lat: , lng:} }
    };
  }

  render() {
    return (
      <div>
        {this.state.qualityStation.slice(0, 2000).map((station, i) => {
          try {
            return (
              <SimpleMarker
                lat={station.latitude}
                lng={station.longitude}
                type="Water Quality station"
                name={station.id}
                idx={i}
              />
            );
          } catch (error) {
            console.log("ERROR FOUND", station);
            return null;
          }
        })}
      </div>
    );
  }
}

export default MultiMarkers;
