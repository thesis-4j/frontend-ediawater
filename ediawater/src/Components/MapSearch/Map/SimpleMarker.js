/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo use map.addListener to send info about position of click and chef if it was outside se example https://github.com/cedricdelpoux/react-google-map
 * @todo

 */

/** jshint {inline configuration here} */
import React, { PureComponent } from "react";

import { Image } from "react-bootstrap";
import { images } from "../../../assets/Images";

const imageProvider = (type, hover) => {
  if (images[type]) {
    if (hover) return images[type].hover;
    return images[type].src;
  }
  return null;
};

export class SimpleMarker extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      infobox: { status: false },
    };
  }

  handleClick = () => {
    console.log(`You clicked on ${this.props.type}`);
    //alert(`You clicked on ${this.props.type}` + this.props.idx)
    this.props.returnClick(this.props.idx);
  };

  render() {
    return (
      <div>
        <div
          className={
            !this.props.icon
              ? this.props.$hover
                ? "circle hover"
                : "circle"
              : "circleInvisible"
          }
          onClick={this.handleClick}
          style={
            this.props.color && !this.props.icon
              ? { backgroundColor: this.props.color }
              : null
          }
        >
          <span className="circleText" title={this.props.type}>
            {this.props.key} {this.props.conf.text.id ? this.props.name : null}{" "}
            {this.props.conf.text.type ? this.props.type : null}
            {this.props.icon && (
              <Image
                className="MarkerImage"
                src={imageProvider("WaterQualityStation", this.props.$hover)}
              />
            )}
          </span>
        </div>
      </div>
    );
  }
}

export default SimpleMarker;
