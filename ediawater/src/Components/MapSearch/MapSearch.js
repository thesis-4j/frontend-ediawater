import React, { Component } from "react";
import { Map } from "./Map/Map";
import { DataContext } from "../../Contexts/DataContext";

export class MapSearch extends Component {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
  }

  componentDidUpdate() {
    console.log("MapSearch.js did update");
  }

  componentDidMount() {
    // subscribe to store
    // will called 2x times but will update only once
    this.props.store.subscribe(() => {
      this.forceUpdate();
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  render() {
    const data = this.props.store.getState();

    return (
      <DataContext.Consumer>
        {(context) => {
          console.log("MapSearch.js re-render");
          return (
            <>
              <Map
                center={data.center}
                infra={data.infra.hidden ? [] : context.infra.data}
                infraConf={{ label: data.infra.label, icon: data.infra.icon }}
                relations={data.relations.hidden ? [] : context.relations.data}
                qualityStation={
                  data.qualityStations.hidden ? [] : context.qualityStation.data
                }
                qualityStationConf={{ ...data.qualityStations }}
                shapefile={context.shapefile.data}
                shapefileConf={data.shapefile}
                mapLayer={data.map.layer}
                windowFilter={data.qualityStations.window}
              />
            </>
          );
        }}
      </DataContext.Consumer>
    );
  }
}
