/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo receive data via props handler
 * @todo  featuresSets: [[{type: features: {name: type:  icon:}] , [] ]
          HIDE ALL SHOULD ALWAYS BE ADDED AS DEFAULT WHEN mapfetures selectNode(node: Node)
 * @todo receive data via props handler
 * @todo receive data via props handler

 */

/** jshint {inline configuration here} */
/* eslint multiline-ternary: 0 */

import React, { PureComponent } from 'react'
import {
  Form,
  DropdownButton,
  Dropdown,
  Container,
  Button,
  Row,
  Spinner
} from 'react-bootstrap'

// import PropTypes from "prop-types";
import { DataContext } from '../Contexts/DataContext'
import ColorPickerMemo from './Commons/ColorPicker'

import {
  showRelations,
  hideRelations,
  showInfra,
  hideInfra,
  showInfraLabel,
  hideInfraLabel,
  showInfraIcon,
  hideInfraIcon,
  showShapefile,
  hideShapefile,
  hideQStation,
  showQStation,
  changeMap,
  EnableFilterQStation,
  DisableFilterQStation,
  selectWStationTextLabel,
  changeWStationColor,
  showWStationIcon,
  hideWStationIcon
} from '../Reducers/Store'

// Constants
const DEFAULT_DATA_SIZE = 10000

export class Settings extends PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      infra: false,
      infraLinks: false,
      featureLayer: false,
      qualityStation: false,
      qualityStationColor: false,
      render: '',
      mapLayer: 'roadmap'
    }

    this.maxDisplayNumber = DEFAULT_DATA_SIZE

    this.renderInput = this.renderInput.bind(this)
    this.handleMaxDisplay = this.handleMaxDisplay.bind(this)
    this.checkLabelNameQStation = this.checkLabelNameQStation.bind(this)
    this.checkLabelTypeQStation = this.checkLabelTypeQStation.bind(this)
    this.onHandleColorPicker = this.onHandleColorPicker.bind(this)
    this.switchHandlerWQStationIcon = this.switchHandlerWQStationIcon.bind(
      this
    )
  }

  /**
This function will receive a event key and will save as state the respective option object
  @param {string} eventKey - event key from the navbar
*/
  renderInput (event) {
    this.setState({ render: event.target.value })
  }

  componentDidUpdate () {
    console.log('MapSettings.js did update')
  }

  loadInfraData (event, saveDataHandler) {
    if (event.target.checked) {
      const data = this.props.store.getState()

      if (data && data.infra.data.length === 0) {
        console.log('Infrastrutures data will be overwritten')
      }

      saveDataHandler()
      this.setState({ infra: true })
    } else {
      this.props.store.dispatch(hideInfra())
      this.setState({ infra: false })
    }
  }

  loadRelations (event, saveDataHandler) {
    if (event.target.checked) {
      const data = this.props.store.getState()

      if (data && data.relations.data.length === 0) {
        console.log('Relationships data will be overwritten')
      }

      saveDataHandler()
      this.setState({ infraLinks: true })
    } else {
      this.props.store.dispatch(hideRelations())
      this.setState({ infraLinks: false })
    }
  }

  loadShapefile (event, saveDataHandler) {
    if (event.target.checked) {
      /*   const data = this.props.store.getState();

      if (data && data.shapefile.data.length === 0) {
        console.log("Relationships data will be overwritten");
      }
*/
      saveDataHandler()
      this.setState({ featureLayer: true })
    } else {
      this.props.store.dispatch(hideRelations())
      this.setState({ featureLayer: false })
    }
  }

  loadQStation (event, saveDataHandler) {
    if (event.target.checked) {
      saveDataHandler()
      this.setState({ qualityStation: true })
    } else {
      this.props.store.dispatch(hideRelations())
      this.setState({ qualityStation: false })
    }
  }

  switchHandlerInfra (event) {
    if (event.target.checked) this.props.store.dispatch(hideInfra())
    else this.props.store.dispatch(showInfra())
  }

  switchHandlerRelations (event) {
    if (event.target.checked) this.props.store.dispatch(hideRelations())
    else this.props.store.dispatch(showRelations())
  }

  switchHandlerInfraIcon (event) {
    if (event.target.checked) this.props.store.dispatch(hideInfraIcon())
    else this.props.store.dispatch(showInfraIcon())
  }

  switchHandlerInfraLabel (event) {
    if (event.target.checked) this.props.store.dispatch(showInfraLabel())
    else this.props.store.dispatch(hideInfraLabel())
  }

  switchHandlerfeatureLayer (event) {
    if (event.target.checked) this.props.store.dispatch(hideShapefile())
    else this.props.store.dispatch(showShapefile())
  }

  switchHandlerQStation (event) {
    if (event.target.checked) this.props.store.dispatch(hideQStation())
    else this.props.store.dispatch(showQStation())
  }

  switchFilterQStation (event) {
    if (event.target.checked) {
      this.props.store.dispatch(DisableFilterQStation())
    } else this.props.store.dispatch(EnableFilterQStation())
  }

  switchHandlerWQStationIcon (event) {
    if (event.target.checked) {
      this.props.store.dispatch(hideWStationIcon())
      this.setState({ qualityStationColor: true })
    } else {
      this.props.store.dispatch(showWStationIcon())
      this.setState({ qualityStationColor: false })
    }
  }

  checkLabelNameQStation (event) {
    const storeState = this.props.store.getState()
    const typeStatus = storeState.qualityStations.text.type
    if (event.target.checked) {
      this.props.store.dispatch(selectWStationTextLabel(true, typeStatus))
    } else {
      this.props.store.dispatch(selectWStationTextLabel(false, typeStatus))
    }
  }

  checkLabelTypeQStation (event) {
    const storeState = this.props.store.getState()
    const idStatus = storeState.qualityStations.text.id
    if (event.target.checked) {
      this.props.store.dispatch(selectWStationTextLabel(idStatus, true))
    } else this.props.store.dispatch(selectWStationTextLabel(idStatus, false))
  }

  onHandleColorPicker (color) {
    this.props.store.dispatch(changeWStationColor(color))
  }

  handleMapLayer (layer) {
    this.props.store.dispatch(changeMap(layer))
    this.setState({ mapLayer: layer })
  }

  handleMaxDisplay (event) {
    try {
      const value = parseInt(String(event.target.value))
      if (value) this.maxDisplayNumber = value
      else this.maxDisplayNumber = DEFAULT_DATA_SIZE

      // console.log("Max", this.maxDisplayNumber);
    } catch (error) {}

    // console.log("Max", this.maxDisplayNumber);
  }

  render () {
    const StoreState = this.props.store.getState()

    return (
      <DataContext.Consumer>
        {(context) => {
          console.log('MapSettings.js re-render')
          return (
            <div className='CheckboxMap'>
              <div className='CheckGroup'>
                <Form>
                  <div>
                    <Form.Check
                      inline
                      label='Infrastructures'
                      type='checkbox'
                      id='inline-check-Infrastrutures-1'
                      onChange={(event) =>
                        this.loadInfraData(event, context.addInfra)}
                    />
                    <Form.Check
                      inline
                      label='Infrastructure Links'
                      type='checkbox'
                      id='inline-check-Relationships-1'
                      onChange={(event) =>
                        this.loadRelations(event, context.addRelations)}
                    />
                    <Form.Check
                      inline
                      label='Map Layers'
                      type='checkbox'
                      id='inline-check-Shapefile-1'
                      onChange={(event) =>
                        this.loadShapefile(event, context.addShapefile)}
                    />
                    <Form.Check
                      inline
                      label='Water Quality Station'
                      type='checkbox'
                      id='inline-water-station-1'
                      onChange={(event) =>
                        this.loadQStation(event, context.addQStation)}
                    />
                    {context.loadingState && (
                      <Spinner animation='grow' size='sm' variant='primary' />
                    )}
                  </div>
                </Form>
              </div>
              <div className='SwitchGroup'>
                {this.state.infra && (
                  <div>
                    <Form.Check
                      type='switch'
                      id='switch-Infrastrutures-1'
                      label='Hide Infrastructures'
                      onChange={(event) => this.switchHandlerInfra(event)}
                    />
                    <div className='SettingsCard'>
                      <Form.Check
                        type='switch'
                        id='switch-Infrastrutures-Icon'
                        label='Infrastructures Icons'
                        onChange={(event) => this.switchHandlerInfraIcon(event)}
                      />
                      <Form.Check
                        type='switch'
                        id='switch-Infrastrutures-Label'
                        label='Infrastructures Labels'
                        onChange={(event) =>
                          this.switchHandlerInfraLabel(event)}
                      />
                    </div>
                  </div>
                )}
                {this.state.infraLinks && (
                  <Form.Check
                    type='switch'
                    id='switch-Relations-1'
                    label='Hide Infra Links'
                    onChange={(event) => this.switchHandlerRelations(event)}
                  />
                )}

                {this.state.featureLayer && (
                  <Form.Check
                    type='switch'
                    id='switch-featureLayer-'
                    label='Hide Map Layers'
                    onChange={(event) => this.switchHandlerfeatureLayer(event)}
                  />
                )}

                {this.state.qualityStation && (
                  <div>
                    <Form.Check
                      type='switch'
                      id='switch-Station-1'
                      label='Hide Quality Station'
                      onChange={(event) => this.switchHandlerQStation(event)}
                    />
                    <div className='SettingsCard'>
                      {' '}
                      <Form.Check
                        inline
                        label='Disable window limits'
                        type='checkbox'
                        id='set-map-window-limit'
                        onChange={(event) => this.switchFilterQStation(event)}
                      />
                      <div className='CoordinatesInputGroup'>
                        <span className='CoordinatesText'>North East</span>
                        <span className='CoordOptionInput'>
                          <Form.Control
                            id='lat-max-coord'
                            size='sm'
                            placeholder='Latitude'
                            className='CoordinatesInput'
                          />
                          <Form.Control
                            id='lng-max-coord'
                            size='sm'
                            placeholder='Longitude'
                            className='CoordinatesInput'
                          />
                        </span>
                      </div>
                      <div className='CoordinatesInputGroup'>
                        <span className='CoordinatesText'>South West</span>
                        <span className='CoordOptionInput'>
                          <Form.Control
                            id='lat-min-coord'
                            size='sm'
                            placeholder='Latitude'
                            className='CoordinatesInput'
                          />

                          <Form.Control
                            id='lng-min-coord'
                            size='sm'
                            placeholder='Longitude'
                            className='CoordinatesInput'
                          />
                        </span>
                      </div>
                      <div className='CoordinatesInputGroup'>
                        <span className='CoordinatesText'>Display</span>

                        <div className='CoordOptionInput'>
                          <Form.Control
                            id='description'
                            size='sm'
                            placeholder='Max. Value'
                            onChange={this.handleMaxDisplay}
                          />
                          <Button
                            variant='info'
                            name='reload'
                            size='sm'
                            onClick={() =>
                              context.sliceQStation(this.maxDisplayNumber)}
                          >
                            Reload
                          </Button>
                        </div>
                      </div>
                      <div>
                        <div style={{ display: 'flex', columnGap: '1rem' }}>
                          Label
                          <span>
                            <Form.Check
                              inline
                              label='Name'
                              type='checkbox'
                              id='inline-check-station-id'
                              value={StoreState.qualityStations.text.id}
                              onChange={(event) =>
                                this.checkLabelNameQStation(event)}
                            />
                            <Form.Check
                              inline
                              label='Type'
                              type='checkbox'
                              value={StoreState.qualityStations.text.type}
                              id='inline-check-station-type'
                              onChange={(event) =>
                                this.checkLabelTypeQStation(event)}
                            />
                          </span>
                        </div>
                        <div
                          style={{
                            display: 'flex',
                            columnGap: '1rem',
                            alignItems: 'center'
                          }}
                        >
                          <Form.Check
                            type='switch'
                            id='switch-wqsations-Icon'
                            label='Disable Icons'
                            onChange={(event) =>
                              this.switchHandlerWQStationIcon(event)}
                          />

                          {this.state.qualityStationColor ? (
                            <ColorPickerMemo
                              handlePickColor={this.onHandleColorPicker}
                              intialValue='#768976'
                              text={false}
                            />
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                <Container>
                  <Row className='optionSelector'>
                    Map Layer
                    <DropdownButton
                      id='map-button'
                      variant='link'
                      title={this.state.mapLayer}
                    >
                      <Dropdown.Item
                        as='button'
                        onSelect={() => this.handleMapLayer('roadmap')}
                      >
                        Road Map
                      </Dropdown.Item>
                      <Dropdown.Item
                        as='button'
                        onSelect={() => this.handleMapLayer('satellite')}
                      >
                        Satellite
                      </Dropdown.Item>
                      <Dropdown.Item
                        as='button'
                        onSelect={() => this.handleMapLayer('terrain')}
                      >
                        Terrain
                      </Dropdown.Item>
                      <Dropdown.Item
                        as='button'
                        onSelect={() => this.handleMapLayer('hybrid')}
                      >
                        Hybrid
                      </Dropdown.Item>
                    </DropdownButton>
                  </Row>
                </Container>
              </div>
              <div>{this.state.render}</div>
            </div>
          )
        }}
      </DataContext.Consumer>
    )
  }
}
