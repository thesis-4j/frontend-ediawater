import React, { Component } from "react";
import { Nav, NavDropdown } from "react-bootstrap";

export class NavButtons extends Component {
  render() {
    return (
      <Nav variant="pills" activeKey="data">
        <NavDropdown title="Files" id="nav-dropdown">
          <NavDropdown.Item eventKey="shapefile" disabled>
            Shapefile Editor
          </NavDropdown.Item>
          <NavDropdown.Item eventKey="spreadsheet" disabled>
            Spreadsheet Importer
          </NavDropdown.Item>
        </NavDropdown>

        <NavDropdown title="View" id="nav-dropdown">
          <NavDropdown.Item
            eventKey="search"
            onClick={this.props.handler.bind(this, "search")}
          >
            Search
          </NavDropdown.Item>
          <NavDropdown.Item
            eventKey="map"
            onClick={this.props.handler.bind(this, "map")}
          >
            Map
          </NavDropdown.Item>
          <NavDropdown.Item
            eventKey="settings"
            onClick={this.props.handler.bind(this, "settings")}
          >
            Map Settings
          </NavDropdown.Item>
          <NavDropdown.Item
            eventKey="files"
            onClick={this.props.handler.bind(this, "files")}
          >
            Console
          </NavDropdown.Item>
        </NavDropdown>

        <NavDropdown title="Tools" id="nav-dropdown">
          <NavDropdown.Item eventKey="simulator" disabled>
            Run Simulation
          </NavDropdown.Item>
          <NavDropdown.Divider />
        </NavDropdown>

        <NavDropdown title="Results" id="nav-dropdown">
          <NavDropdown.Item eventKey="report" disabled>
            Export Report
          </NavDropdown.Item>
        </NavDropdown>

        <Nav.Item>
          <Nav.Link href="https://gitlab.com/thesis-4j/thesis-project/-/issues">
            Report Issues
          </Nav.Link>
        </Nav.Item>
      </Nav>
    );
  }
}
