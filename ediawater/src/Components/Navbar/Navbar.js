import React, { Component } from "react";
import { Image, Navbar } from "react-bootstrap";
import { NavButtons } from "./NavButtons";
import { ThemeSwitch } from "../Commons/ThemeSwitch";

const NavInfo = ({ session, version }) => (
  <span className="NavInfo">
    <div>Last Session {session}</div>
    <div>Licence {version}</div>
  </span>
);

export class Nav extends Component {
  static defaultProps = {
    theme: "",
  };

  componentDidUpdate() {
    console.log("Navbar.js did update");
  }

  render() {
    console.log("Navbar.js re-render");
    return (
      <div className={"Nav" + this.props.theme}>
        <Navbar className="py-0" variant="dark">
          <NavButtons handler={this.props.AddItemHandler} />

          <Navbar.Collapse className="justify-content-end">
            <ThemeSwitch onChangeTheme={this.props.handleChangeThemeHandler} />
            <span>
              <Image
                className="Avatar"
                roundedCircle
                src="https://source.unsplash.com/random/40x40"
              />
            </span>
            <span>
              <NavInfo session="10:12:59 UTC" version="Free" />
            </span>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
