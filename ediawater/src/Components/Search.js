/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo receive data via props handler
 * @todo
 */

/** jshint {inline configuration here} */

import React, { PureComponent } from 'react'
import { Form, Card } from 'react-bootstrap'
import { setMapCenter } from '../Reducers/Store'
import { DataContext } from '../Contexts/DataContext'

let data = []

export class Search extends PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      render: '',
      results: []
    }

    this.handleRenderInput = this.handleRenderInput.bind(this)
  }

  componentDidUpdate () {
    console.log('Search.js did updated')
    // console.log(this.state.results);
  }

  /**
    This function will receive a event key and will save as state the respective option object
      @param {string} eventKey - event key from the navbar
  */
  handleRenderInput (event) {
    const substring = event.target.value
    const result = data.filter(function (elem) {
      if (
        elem.name.toLowerCase().indexOf(substring.toLowerCase()) !== -1 ||
        elem.type.toLowerCase().indexOf(substring.toLowerCase()) !== -1
      ) {
        return true
      }
      return false
    })
    this.setState({ results: result, render: result.length })
  }

  render () {
    return (
      <DataContext.Consumer>
        {(context) => {
          data = context.infra.data
          console.log('Search.js re-render')
          return (
            <div className='Search'>
              <div className='SearchInput'>
                <Form.Control
                  id='description'
                  placeholder='Search'
                  onChange={this.handleRenderInput}
                  className='SearchInput'
                  size='sm'
                />
              </div>
              <div className='SearchCard'>
                {this.state.results.map((finds, i) => {
                  return (
                    <Card
                      key={i}
                      onClick={() =>
                        this.props.store.dispatch(
                          setMapCenter(finds.location[1], finds.location[0])
                        )}
                      className='SearchCardDisplay'
                      text='dark'
                      border='success'
                    >
                      <Card.Body>
                        <Card.Title>{finds.name}</Card.Title>
                        Type: {finds.type} <br />
                        Sub-type: {finds.subtype} <br />
                      </Card.Body>
                    </Card>
                  )
                })}
              </div>
            </div>
          )
        }}
      </DataContext.Consumer>
    )
  }
}
