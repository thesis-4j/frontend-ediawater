import React, { Component, createContext } from 'react'
import axios from 'axios'

// Constants
let API_SERVER_URL = null
const DEFAULT_DATA_SIZE = 10000

export const DataContext = createContext()

class DataContextProvider extends Component {
  constructor (props) {
    super(props)
    this.state = {
      relations: { data: [] },
      infra: { data: [] },
      shapefile: { data: [], raw: [] },
      qualityStation: { data: [] },
      loadingState: false
    }
    API_SERVER_URL = process.env.REACT_APP_API_HOST
      ? process.env.REACT_APP_API_HOST
      : 'http://localhost:5001/'

    this.sliceQualityStation = this.sliceQualityStation.bind(this)
  }

  getWaterNeo4jAPI = () => {
    this.setState({ loadingState: true })
    axios
      .get(API_SERVER_URL + 'api/waternode')
      .then((res) => {
        const { data } = res
        console.log('API waternodes', data)
        if (typeof data !== 'undefined') {
          this.setState({
            infra: { data: data[0].waternodes },
            loadingState: false
          })
          // console.log(data[0]["waternodes"][0].location[0])
        }
      })
      .catch((error) => {
        console.log(error)
        this.setState({ loadingState: false })
      })
  };

  getRelNeo4jAPI = () => {
    this.setState({ loadingState: true })
    axios
      .get(API_SERVER_URL + 'api/waternode/relation')
      .then((res) => {
        const { data } = res
        if (typeof data !== 'undefined') {
          this.setState({ relations: { data: data }, loadingState: false })
        }
      })
      .catch((error) => {
        console.log(error)
        this.setState({ loadingState: false })
      })
  };

  getShapefile = () => {
    axios.get(API_SERVER_URL + 'api/map/watershed/geojson/').then((res) => {
      const { data } = res
      console.log('receveide shp', data)
      if (typeof data !== 'undefined' && data) {
        this.setState({ shapefile: { data: data } })
      }
    })
  };

  getQualityStation = () => {
    this.setState({ loadingState: true })
    axios
      .get(API_SERVER_URL + 'api/water-quality/station/')
      .then((res) => {
        const { data } = res
        // console.log('receive Station', data[0].station)
        if (typeof data !== 'undefined') {
          this.setState({
            qualityStation: {
              ...this.state.qualityStation,
              raw: data[0].station,
              data: data[0].station,
              size: data[0].station.length
            },
            loadingState: false
          })
        }
        console.log('Quality Stations Added')
      })
      .catch((error) => {
        console.log(error)
        this.setState({ loadingState: false })
      })
  };

  // check if northeast/southwest is defined - if it's defined, should slice from crop data to data  and not from raw
  // maybe make sense to save the crop data to other variable, like raw variable
  // slice data to data
  sliceQualityStation = (size = DEFAULT_DATA_SIZE) => {
    if (this.state.qualityStation.raw.length) {
      let sliceSize = size

      if (size > this.state.qualityStation.size) {
        sliceSize = this.state.qualityStation.size
      }

      const parseData = this.state.qualityStation.raw.slice(0, sliceSize)
      this.setState({
        qualityStation: { ...this.state.qualityStation, data: parseData }
      })
    }
  };

  // filter raw data to data
  // can pre filter or check only mapbounds that will only send redux option to console.js parse data
  // on didUpdate of Map will update/filter the console data

  filterQualityStation = (nortEast, southWest) => {
    try {
      if (
        (nortEast != null || typeof nortEast !== 'undefined') &&
        (southWest != null || typeof southWest !== 'undefined')
      ) {
        const parseData = this.state.qualityStation.data.filter(
          (marker) =>
            marker.latitude < window.nortEast.lat &&
            marker.latitude > window.southWest.lat &&
            marker.longitude < window.nortEast.lng &&
            marker.longitude > window.southWest.lng
        )

        this.setState({
          qualityStation: { ...this.state.qualityStation, data: parseData }
        })
      } else if (
        (nortEast == null || typeof nortEast === 'undefined') &&
        (southWest != null || typeof southWest !== 'undefined')
      ) {
        const parseData = this.state.qualityStation.data.filter(
          (marker) =>
            marker.latitude > window.southWest.lat &&
            marker.longitude > window.southWest.lng
        )

        this.setState({
          qualityStation: { ...this.state.qualityStation, data: parseData }
        })
      } else if (
        (nortEast != null || typeof nortEast !== 'undefined') &&
        (southWest == null || typeof southWest === 'undefined')
      ) {
        const parseData = this.state.qualityStation.data.filter(
          (marker) =>
            marker.latitude < window.nortEast.lat &&
            marker.longitude < window.nortEast.lng
        )

        this.setState({
          qualityStation: { ...this.state.qualityStation, data: parseData }
        })
      } else {
        alert('Missing coordinates on users window limits')
      }
    } catch (error) {
      alert('Missing input parameters at filter data option')
    }
  };

  render () {
    return (
      <DataContext.Provider
        value={{
          ...this.state,
          addRelations: this.getRelNeo4jAPI,
          addInfra: this.getWaterNeo4jAPI,
          addShapefile: this.getShapefile,
          addQStation: this.getQualityStation,
          sliceQStation: this.sliceQualityStation
        }}
      >
        {this.props.children}
      </DataContext.Provider>
    )
  }
}

export default DataContextProvider
