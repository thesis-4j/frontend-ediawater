/**
 * Description. (use period)
 *
 *
 * @link    https://gitlab.com/thesis-4j/frontend-ediawater/-/issues
 * @author  Bruno Figueiredo
 * @see     https://jsdoc.app
 * @see     https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/
 *
 * @todo
 * @todo
 */

/** jshint {inline configuration here} */
import React from 'react'
import { MapSearch } from './Components/MapSearch/MapSearch'
// import { Console } from './Components/Console/Console'
import { Search } from './Components/Search'
import { Settings } from './Components/MapSettings'

// functions
import Store from './Reducers/Store'

const Console = React.lazy(() => import('./Components/Console/Console'))

/**
Insert elements by adding a dictionary with:name,comp,key
layout = {x: , y: , w: , h: , i: ,static: }
element = {name: comp:  }

key which we are passing in the div must match with the ids(i)
passed into the objects inside layout, the positioning of the cards will be determined by this ids

  @param {string} name - name of the component
  @param {object} comp - component itself
  @param {string} key - name to be index by navbar

*/
const items = [
  { name: 'search', comp: (theme) => <Search store={Store} />, key: 'search' },
  {
    name: 'map_settings',
    comp: (theme) => <Settings store={Store} />,
    key: 'settings'
  },
  { name: 'map', comp: (theme) => <MapSearch store={Store} />, key: 'map' },
  {
    name: 'files',
    comp: (theme) => (
      <React.Suspense fallback={<div>Loading... </div>}>
        <Console theme={theme} />
      </React.Suspense>
    ),
    key: 'files'
  }
]

const storedLayouts = [
  { x: 0, y: 0, w: 4, h: 10, i: 'search', static: true },
  { x: 0, y: 10, w: 4, h: 14, i: 'map_settings', static: true },
  { x: 4, y: 0, w: 8, h: 24, i: 'map', static: true },
  { x: 0, y: 24, w: 12, h: 12, i: 'files', static: true }
]

export const AppLayout = {
  isDraggable: true,
  isResizable: true,
  preventCollision: false,
  cols: 12,
  rowHeight: 30,
  layout: storedLayouts,
  items: items
}
