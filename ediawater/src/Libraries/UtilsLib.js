import React from "react";

export function isEmpty(obj = {}) {
  for (const i in obj) return false;
  return true;
}

export function ExistsInList(List, specs, fn) {
  for (var i = List.length - 1; i >= 0; i--) {
    if (fn(List[i], specs)) {
      return true;
    }
  }

  return false;
}

export function propertyExistsIn(property, propertyList, keyComp) {
  for (var i = propertyList.length - 1; i >= 0; i--) {
    try {
      if (propertyList[i][keyComp] === property[keyComp]) {
        return true;
      }
    } catch (err) {}
  }

  return false;
}

export function remDups(objList, keys, strict = true, parse = false) {
  let x = [];
  let y = [];

  let final = [];
  let duplicateFlag = false;
  // should only pass condition instead repeating code
  if (strict) {
    console.log("remDups in strict mode");
    for (var i = objList.length; i >= 0; i--) {
      for (let j = 0; j < x.length; j++) {
        if (
          objList[i][keys[0]] === x[j] ||
          (objList[i][keys[0]] === x[j] && objList[i][keys[1]] === y[j])
        ) {
          duplicateFlag = true;
          break;
        }
      }
      if (
        !duplicateFlag &&
        objList[i] &&
        objList[i][keys[0]] &&
        objList[i][keys[1]]
      ) {
        final.push(objList[i]);
        x.push(objList[i][keys[0]]);
        y.push(objList[i][keys[1]]);
      }
      duplicateFlag = false;
    }
  } else {
    for (var t = objList.length; t >= 0; t--) {
      for (let j = 0; j < x.length; j++) {
        if (objList[t][keys[0]] === x[j] && objList[t][keys[1]] === y[j]) {
          duplicateFlag = true;
          break;
        }
      }
      if (
        !duplicateFlag &&
        objList[t] &&
        objList[t][keys[0]] &&
        objList[t][keys[1]]
      ) {
        final.push(objList[t]);
        x.push(objList[t][keys[0]]);
        y.push(objList[t][keys[1]]);
      }
      duplicateFlag = false;
    }
  }

  //console.log("After remDups", final);

  if (parse) {
    let notEmpty = [];
    for (var z = final.length - 1; z >= 0; z--) {
      if (
        final[z][keys[1]] !== "" &&
        final[z][keys[1]] !== " " &&
        final[z][keys[1]] !== null
      ) {
        //console.log("val", final[z], parseFloat(final[z][keys[1]]));
        let val = parseFloat(final[z][keys[1]]);
        if (val) {
          final[z][keys[1]] = val;
          notEmpty.push(final[z]);
        }
      }
    }
    final = [...notEmpty];
    notEmpty = null;
  }

  //console.log("After parse ", final);
  x = null;
  y = null;
  duplicateFlag = null;

  return final;
}

export const sliceChunk = (arr, size) =>
  Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
    arr.slice(i * size, i * size + size)
  );

export const data = (type) => <>{type}</>;

export function pushSorted(list, element, fnGetVar) {
  var i = 0;
  var j = list.length - 1;
  var h;
  var c = false;
  //console.log("test",fnGetVar(list[j]))
  if (fnGetVar(element) > fnGetVar(list[j])) {
    list.push(element);
    return list;
  }
  if (fnGetVar(element) < fnGetVar(list[i])) {
    list.splice(i, 0, element);
    return list;
  }
  while (c === false) {
    h = ~~((i + j) / 2); //a faster h=Math.floor((i+j)/2);
    if (fnGetVar(element) > fnGetVar(list[h])) {
      i = h;
    } else {
      j = h;
    }
    if (j - i <= 1) {
      list.splice(j, 0, element);
      c = true;
    }
  }
  return list;
}

export const getObjSubset = (obj, keys) =>
  keys.reduce((a, b) => {
    a[b] = obj[b];
    return a;
  }, {}); // ex keys = ['keyone', 'keytwo']
