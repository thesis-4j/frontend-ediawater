const initialState = {
  relations: { hidden: false, data: [] },
  infra: {
    hidden: false,
    icon: true,
    label: false,
    text: { id: false, type: false },
    data: []
  },
  shapefile: { hidden: false },
  qualityStations: {
    hidden: false,
    window: true,
    label: false,
    icon: true,
    text: { id: false, type: false },
    color: '#768976'
  },
  center: {
    lat: 38.26,
    lng: -7.61
  },
  map: { layer: 'roadmap' }
}

function MapReducer (state = initialState, action) {
  switch (action.type) {
    case 'relations/SHOW': {
      // console.log('SHOW')
      return {
        ...state,
        relations: { ...state.relations, hidden: false }
      }
    }
    case 'relations/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        relations: { ...state.relations, hidden: true }
      }
    }
    case 'qualityStations/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        qualityStations: { ...state.qualityStations, hidden: true }
      }
    }
    case 'qualityStations/SHOW': {
      // console.log('SHOW')
      return {
        ...state,
        qualityStations: { ...state.qualityStations, hidden: false }
      }
    }

    case 'qualityStations/label/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        qualityStations: { ...state.infra, label: false }
      }
    }

    case 'qualityStations/label/SHOW': {
      // console.log('HIDE')
      return {
        ...state,
        qualityStations: { ...state.infra, label: true }
      }
    }

    case 'qualityStations/label/TEXT': {
      // console.log('label/tezt', action.payload)
      return {
        ...state,
        qualityStations: { ...state.qualityStations, text: action.payload }
      }
    }

    case 'qualityStations/color': {
      // console.log('new color', action.payload)
      return {
        ...state,
        qualityStations: {
          ...state.qualityStations,
          color: action.payload,
          icon: false
        }
      }
    }
    case 'qualityStations/ENABLE_FILTER': {
      // console.log('HIDE')
      return {
        ...state,
        qualityStations: { ...state.qualityStations, window: true }
      }
    }
    case 'qualityStations/DISABLE_FILTER': {
      // console.log('SHOW')
      return {
        ...state,
        qualityStations: { ...state.qualityStations, window: false }
      }
    }

    case 'qualityStations/icon/HIDE': {
      // console.log('qualityStations HIDE')
      return {
        ...state,
        qualityStations: { ...state.qualityStations, icon: false }
      }
    }
    case 'qualityStations/icon/SHOW': {
      // console.log('qualityStations SHOW')
      return {
        ...state,
        qualityStations: { ...state.qualityStations, icon: true }
      }
    }

    case 'shapefile/SHOW': {
      // console.log('SHOW')
      return {
        ...state,
        shapefile: { ...state.relations, hidden: false }
      }
    }
    case 'shapefile/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        shapefile: { ...state.relations, hidden: true }
      }
    }
    case 'infra/SHOW': {
      // console.log('SHOW')
      return {
        ...state,
        infra: { ...state.infra, hidden: false }
      }
    }
    case 'infra/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        infra: { ...state.infra, hidden: true }
      }
    }
    case 'infra/icon/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        infra: { ...state.infra, icon: false }
      }
    }
    case 'infra/icon/SHOW': {
      // console.log('HIDE')
      return {
        ...state,
        infra: { ...state.infra, icon: true }
      }
    }
    case 'infra/label/HIDE': {
      // console.log('HIDE')
      return {
        ...state,
        infra: { ...state.infra, label: false }
      }
    }

    case 'infra/label/SHOW': {
      // console.log('HIDE')
      return {
        ...state,
        infra: { ...state.infra, label: true }
      }
    }

    case 'infra/label/TEXT': {
      return {
        ...state,
        infra: { ...state.infra, text: action.payload }
      }
    }

    case 'center/SET': {
      // console.log('center/SET', action.payload)

      return {
        ...state,
        center: { lat: action.payload.lat, lng: action.payload.lng }
      }
    }
    case 'map/CHANGE': {
      // console.log('map/CHANGE', action.payload)

      return {
        ...state,
        map: { layer: action.payload.layer }
      }
    }

    default: {
      return state
    }
  }
}

export default MapReducer
