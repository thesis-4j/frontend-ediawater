import { createStore } from 'redux'
import { createAction } from '@reduxjs/toolkit'

import MapReducer from './MapReducer'

const initialState = {
  relations: { hidden: false, data: [] },
  infra: {
    hidden: false,
    icon: true,
    label: false,
    text: { id: false, type: false },
    data: []
  },
  shapefile: { hidden: false },
  qualityStations: {
    hidden: false,
    window: true,
    label: false,
    icon: true,
    text: { id: false, type: false },
    color: '#768976'
  },
  center: {
    lat: 38.26,
    lng: -7.61
  },
  map: { layer: 'roadmap' }
}

const Store = createStore(MapReducer, initialState)
console.log('Store Inital Value', Store.getState())

export const showRelations = createAction('relations/SHOW')
export const hideRelations = createAction('relations/HIDE')

export const showInfra = createAction('infra/SHOW')
export const hideInfra = createAction('infra/HIDE')

export const showShapefile = createAction('shapefile/SHOW')
export const hideShapefile = createAction('shapefile/HIDE')

export const changeMap = createAction('map/CHANGE', function prepare (mapLayer) {
  return {
    payload: { layer: mapLayer }
  }
})

export const showQStation = createAction('qualityStations/SHOW')
export const hideQStation = createAction('qualityStations/HIDE')
export const EnableFilterQStation = createAction(
  'qualityStations/ENABLE_FILTER'
)
export const DisableFilterQStation = createAction(
  'qualityStations/DISABLE_FILTER'
)

export const showInfraIcon = createAction('infra/icon/SHOW')
export const hideInfraIcon = createAction('infra/icon/HIDE')
export const showInfraLabel = createAction('infra/label/SHOW')
export const hideInfraLabel = createAction('infra/label/HIDE')

export const showWStationLabel = createAction('qualityStations/label/SHOW')
export const hideWStationLabel = createAction('qualityStations/label/HIDE')

export const showWStationIcon = createAction('qualityStations/icon/SHOW')
export const hideWStationIcon = createAction('qualityStations/icon/HIDE')

export const changeWStationColor = createAction(
  'qualityStations/color',
  function prepare (color) {
    return {
      payload: color
    }
  }
)

export const selectInfraTextLabel = createAction(
  'infra/label/TEXT',
  function prepare (id, type) {
    return {
      payload: { id: id, type: type }
    }
  }
)
export const selectWStationTextLabel = createAction(
  'qualityStations/label/TEXT',
  function prepare (id, type) {
    return {
      payload: { id: id, type: type }
    }
  }
)

export const setMapCenter = createAction(
  'center/SET',
  function prepare (lat, lng) {
    return {
      payload: { lat: lat, lng: lng }
    }
  }
)

export default Store
