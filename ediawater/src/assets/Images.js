import dam from "./images/dam-64.png";
import damHover from "./images/dam-64-hover.png";
import storage from "./images/storage-tank-64.png";
import storageHover from "./images/storage-tank-64-hover.png";
import pump from "./images/pump-64.png";
import pumpHover from "./images/pump-64-hover.png";
import pipe from "./images/water-pipe-64.png";
import pipeHover from "./images/water-pipe-64-hover.png";

import waterQuality from "./images/water-collection-100.png";
import waterQualityHover from "./images/water-collection-100-hover.png";

import fileCancel from "./images/files/cancel-48.png";
import fileOk from "./images/files/ok-48.png";
import fileUparrow from "./images/files/uparrow-48.png";

export const images = {
  Barragem: {
    src: dam,
    hover: damHover,
  },
  Reservatório: {
    src: storage,
    hover: storageHover,
  },

  "Estação Elevatória": {
    src: pump,
    hover: pumpHover,
  },

  Conduta: {
    src: pipe,
    hover: pipeHover,
  },

  WaterQualityStation: {
    src: waterQuality,
    hover: waterQualityHover,
  },

  File: {
    ok: fileOk,
    cancel: fileCancel,
    up: fileUparrow,
  },
};

export default images;
