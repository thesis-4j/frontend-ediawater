const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
//const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  entry: ["@babel/polyfill", "./src/index.js"], //@babel/polyfill for async calls
  mode: "development",
  target: "web",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "webpack-bundle.js",
    publicPath: path.resolve(__dirname, "/build/"),
  },
  /*  optimization: {
    minimizer: [new UglifyJsPlugin({ sourceMap: true, test: /\.js(\?.*)?$/i })],
  },
  devtool: "source-map", */
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: ["/node_modules/", "/src/styles.css"],
        query: { compact: false },
      },
      {
        test: /\.(css)$/,
        loaders: ["style-loader", "css-loader"],
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: "url-loader",
        },
      },
    ],
  },
  node: {
    console: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./public/index.html", // reference index.html file
      filename: "./index.html", // the name and location of referenced file at output folder
      favicon: "./public/dam.ico",
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: "./public/manifest.json", to: "manifest.json", toType: "file" },
      ],
    }),
  ],
  devServer: {
    publicPath: path.resolve(__dirname, "/build/"),
    contentBase: [path.join(__dirname, "/build/")], // path that contain webpack-bundle
    port: 8085, // port where the app will be accessible
    hot: true,
    open: true, // open web browser after compiled
    compress: true,
    historyApiFallback: true,
    /* proxy: [
      {
        context: ["/api"],                  // endpoint which you want to proxy the provider
        target: "http://localhost:3000",     // your main server address - react app provider
      },
    ], */
  },
};
